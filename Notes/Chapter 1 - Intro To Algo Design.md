# Introduction To Algorithm Design

### 1.1. Robot Tour Optimization

I/p: A set S of n points on the plane
O/p: What is the shortest cycle tour that visits each point in the Set S?

This is the travelling sales man problem. No efficient algorithm exists for this sort of problem.

_Algorithms_: always produce a correct result
_Heuristics_: usually do a good job but without providing any guarantee


### 1.2 Movie Scheduling Problem

 Imagine you are a highly-in- demand actor, who has been presented with offers to star in n different movie projects under development. Each offer comes specified with the first and last day of filming. To take the job, you must commit to being available throughout this entire period. Thus you cannot simultaneously accept two jobs whose intervals overlap.

I/p: A set I of n intervals on the line 
O/p: What is the largest subset of mutually non-overlapping intervals which can be selected from I?

```

		_____Tarjan Of Jungle_____   ___The Four Voulme Problem___
___The President Algorist______ __Steiner'sTree___         __ProcessTreminated___
								__Haltins State__.        __Prog_
	__Discreet Maths__						                        __CalculatedBets__

```

*Solution:*
 
>If we always pick from the group of movies that is overlapping, the item/movie that completes first. Then I guess we have an algo at our hand.

ALGO:

* Set S = ["Tarjan Of Jungle", "The President Algorist", ...]

* Sort S on completion dates.
* Accept the one which has earliest completion date and delete all movies whcih intersect with selected movie.

```
Sort S

while(S != nil) do
	Pick first one
	result.append(s.first)
	delete s.first
	delete all movies from S which overlaps with S.first

return result	
```

### 1.4 Modeling the Problem
Modeling is art of understanding your problem in terms of already well defined, well understood problems.

### 1.4.1 Combinatorial Objects

Important combinatorial objects are: permutations, combinations, and subsets of a set.

### 1.5 War Story: Psychic Modeling

I am not even able to understand the problem, let alone its solution. I searched on net and people have suggested to skip this for now and after reading some more, come back and try to implement the solution.

## 1.7 Exercises

*Finding Counterexamples*

**1-1** [3] Show that a + b can be less than min(a, b)
*Sol:*
> if both a and b are negative numbers

**1-2** [3] Show that a × b can be less than min(a, b). 
*Sol:*
> when either of a or b is negative

*Estimation*

**1-19.** [3] Do all the books you own total at least one million pages? How many total pages are stored in your school library?
*Sol:*
> Total books I own, is approx 10. Average number of pages in every book is 500.
> Total nmbr of pages = 500*10 = 5000
> I can safely say I dont own one million pages.

For school library?
> we need info about average number of books in an Indian school library.
> Lets say 12000 books are ther in average and 300 is number of pages.
> => 12000*300 = 3600000

**1-21** How many hours are one million seconds? How many days? Answer thesequestions by doing all arithmetic in your head.
*Sol:*
> 1000000 s =~ 1.666*10^4 minutes
> 16666 m =~ 18666-2000 m =~ 311.1-30 h =~ 300-30 = 270 h

**1-23** [3] Estimate how many cubic miles of water flow out of the mouth of the Mississippi River each day. Do not look up any supplemental facts. Describe all assumptions you made in arriving at your answer.

*Sol:*
> Lets try to create a cube at the mouth of the missisipi and use **l\*b\*h** formula to find teh cubic miles of water.
> height = 0.01 mile, bredth = 1 mile, length = 10 miles in an hour
> lbh = 0.01 * 1 * 100 = 0.1 cubic miles per hour
> 0.1 * 24 = 2.4 cubic miles each day

*Interview Problems*

**1-28.** [5] Write a function to perform integer division without using either the / or * operators. Find a fast way to do it.

*Sol:*
> First thing that comes to mind is to do it using repeated subtraction

 ```
 while N ≥ D do
  	N := N − D
 end
 return N
 ```

**1-29.** [5] There are 25 horses. At most, 5 horses can race together at a time. You must determine the fastest, second fastest, and third fastest horses. Find the minimum number of races in which this can be done.

*Sol:*
> 1. Initial 5 Races to determine 5 winners.
> 2. 6th race among winners to determine the fastest  
> 3. Can discard all 10 horses of the groups where winner horse came 4th and 5th in the 6th race. 
> 4. we have 2nd and 3rd horses from sixth race and their groups alongwith 4 horses from the group where winner came first in 6th race. there is no point running winner of 6th race again. he is fastest in any case. we have to detrmine 2nd and 3rd fastest.
> 5. we are going to have 7th race with these horse:
> 	 * third horse as he can still be third fastest. discard other 4 horses from his group.
>	 * 2nd horse and 2nd fastest from his group as they still can be 2nd and 3rd fastest. discard other 3 horses
>	 * 2nd, 3rd from winner horse group.

=> Total number of race = 7
 