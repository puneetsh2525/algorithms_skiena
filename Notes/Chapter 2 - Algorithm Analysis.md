##2.10 Exercises

**Interview Problems**

**2-43.** [5] You are given a set S of n numbers. You must pick a subset S′ of k numbers from S such that the probability of each element of S occurring in S′ is equal (i.e., each is selected with probability k/n). You may make only one pass over the numbers. What if n is unknown?

*Sol:*

> * I didn’t have solution to this problem, so I used Google which pointed me to this SO [link][solink]
> * The answer says that this problem is very famous and to solve it we have a technique that is called [Reservoir Sampling][reservoir-sampling].
> * Apparently, this problem is very important to solve for Data Scientists. See this blog [link][bloglink1] or this [link][bloglink2].
> * _Reservoir Sampling is an algorithm for sampling elements from a stream of data. Imagine you are given a really large stream of data elements, e.g. queries on google searches in May. Your goal is to return a random sample of 1000 elements evenly distributed from the original stream. How would you do it? Simple solution is to generate random numbers between 0 and N-1 and then retrieving elements at those indices. But this would not possible if N is large or unknown and does not even fit in any memory._
> * Algo for Reservoir Sampling:
> 	1. Make a reservoir(array) of k elements(in above case, k = 1000) and fill it with the first k elements. This way if n = k, then algo works. This is the base case.
> 	2. Next, we want to process the ith element(starting from k+1), such that at the end of processing that step, the 1000 elements in your reservoir are randomly sampled amongst the i elements you have seen so far.
> 	3. Now one by one consider all items from (k+1)th item to nth item
> 
> 	
> 	 ```
> 	 Generate a random number from 0 to i where i is index of current item in stream[]. 
> 	 Let the generated random number is j.
> 
> 	 If j is in range 0 to k-1, replace reservoir[j] with arr[i].
> 	 ```			 
> 
> 	4. To know about the proof for this solution read the second link(in third point). 

[solink]:http://stackoverflow.com/questions/30546595/set-s-of-n-numbers-have-a-subset-with-the-probability-of-each-element-of-s-occ

[reservoir-sampling]:https://en.wikipedia.org/wiki/Reservoir_sampling

[bloglink1]:http://blog.cloudera.com/blog/2013/04/hadoop-stratified-randosampling-algorithm/

[bloglink2]:https://gregable.com/2007/10/reservoir-sampling.html


**2-44**. [5] We have 1,000 data items to store on 1,000 nodes. Each node can store copies of exactly three different items. Propose a replication scheme to minimize data loss as nodes fail. What is the expected number of data entries that get lost when three random nodes fail?

*Sol:*
> 
> To solve this problem, I thought every node should save the preceding and the next item i.e `i-1, i, i+1`.
> Lets say we have saved nodes using above algo in format
> `1000 1 2`, `1 2 3`, `2 3 4`, `3 4 5`, `4 5 6`.
> Even if nodes, 2, 3, 4 fail, we will loose only item for data 3.

**2-45.** [5] Consider the following algorithm to find the minimum element in an array of numbers A[0,...,n]. One extra variable tmp is allocated to hold the current minimum value. Start from A[0]; ”tmp” is compared against A[1], A[2], ..., A[N] in order. When A[i] < tmp, tmp = A[i]. What is the expected number of times that the assignment operation tmp = A[i] is performed?
 
*Sol:*

> ~~In worst case items inside array is sorted in descending order i.e. `5, 4, 3, 2, 1`, hence `tmp = A[i]` will happen **n-1** times.~~
> 
> This is a problem for probabiliy. Probablity of every item to be minimum is 1/n.
> 
> => sum of 1/i n times ~ *ln* n (log n). 


**2-46.** [5] You have a 100-story building and a couple of marbles. You must identify the lowest floor for which a marble will break if you drop it from this floor. How fast can you find this floor if you are given an infinite supply of marbles? What if you have only two marbles?

*Sol:*

> ###### Infinite No. Of Eggs 
> Use binary search. 
> 
> > * Drop at 50th floor.
> > * If it breaks then drop at 25th floor.
> > * If it breaks then drop at 13th floor
> > * If it does not break then drop at 19th floor
> > * If it does not break then drop at 22nd floor.
> > * If it does not break then drop at 23rd floor.
> > * If it does not break then drop at 24th floor.
> > * If it does not break then answer is 25.
> 
> This way we can identify lowest floor by using binary search. But this way of finding lowest floor is appropriate only if we employ binary search. 
> 
> ###### Two Eggs 
> If total number of eggs is `two` this means we can not risk our second egg.
> If we start dropping first egg at 10th floor, and the egg breaks then we would have to drop the second egg linearly starting from 1st floor and then going up.
> If egg does not break at 10th floor, then we can drop at floor 20th and see the result. This way the worst case scenario will be if threshold floor is 100, because we will have to drop egg 19 times to find this floor.
> __How?__ 10th, 20th, 30th, 40th, 50th, 60th, 70th, 80th, 90th, 100th 91, 92, 93, 94, 95, 96, 97, 98, 99.
> 
> But there is a better solution available. The main reason why it takes such a large number of drops in the worst case with our approach above (19 drops) is because in order to test out the higher floors of the building we have to start at the lower floors of the building, and at that point we have already used a large number of drops just to get to that point.
> 
> `x + (x-1) + (x-2) + (x-3) + ... + 1`
> => x = 14.

**2-47.** [5] You are given 10 bags of gold coins. Nine bags contain coins that each weigh 10 grams. One bag contains all false coins that weigh one gram less. You must identify this bag in just one weighing. You have a digital balance that reports the weight of what is placed on it.

*Sol:*

Maths to the resue.

> * Find an empty bag(E).
> * Place 1 coin from 1st bag in E.
> * Place 2 coins from 2nd bag in E.
> * Place 3 coins from 3rd bag in E.
> * ...
> * ...
> * Place 10 coins from 10th bag in E.
> 

Total sum of E bag, if all coins are 10 gm,
`10 + 2(10) + 3(10) + ... + 10(10) = 550` // sum of AP formula
But because of fake coins, this sum would be different. lets say sum is 545.
`550-545 = 5`. This means bag 5 is full of fake coins.

**2-48.** [5] You have eight balls all of the same size. Seven of them weigh the same, and one of them weighs slightly more. How can you find the ball that is heavier by using a balance and only two weighings?*Sol:*

> * Divide eigh balls in gropu of 3, 3 and 2 balls.
> * Weigh 3 and 3 balls on scale.
> * If left/right balance is weighing more, then weigh any 2 balls from the group.
> * If on first weighing, scale is balanced then weigh group of two balls to find the heavier ball.

**2-49.** [5] Suppose we start with n companies that eventually merge into one big company. How many different ways are there for them to merge?

*Sol:*


**2-51.** Six pirates must divide $300 dollars among themselves. The division is to proceed as follows. The senior pirate proposes a way to divide the money. Then the pirates vote. If the senior pirate gets at least half the votes he wins, and that division remains. If he doesn't, he is killed and then the next senior-most pirate gets a chance to do the division. Now you have to tell what will happen and why (i.e., how many pirates survive and how the division is done)? All the pirates are intelligent and the first priority is to stay alive and the next priority is to get as much money as possible.

*Sol:*

This problem can be solved by thinking in terms of two pirates first and then involving others. Kind of teaches you the importance of divide and rule.

> * If we have only two pirates, then leader will not give anything to the other pirate(as he already has 50% of the vote). `1, 2 -> 300, 0`
> * Include 3rd more pirate. The leader needs his vote. He can offer him 1 dollar, because that would be enough for him to give his vote. If he denies and leader dies then the second person will give him nothing. For him getting 1 dollar is best case scenario. `1, 2, 3 -> 299, 0, 1`
> Including 4th pirate now. The leader does not need his vote. So, `1, 2, 3, 4 -> 299, 0 , 1, 0`.
> Including 5th pirate, the leader again needs his vote and will offer him the same deal. If he denies he is going to be the 4th pirate and will get nothing. `1, 2, 3, 4, 5 -> 299, 0 , 1, 0, 1`.
> Sixth pirate is immaterial. `1, 2, 3, 4, 5, 6 -> 299, 0 , 1, 0, 1, 0`.






