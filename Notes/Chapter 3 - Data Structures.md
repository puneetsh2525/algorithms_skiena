# Data Structures

### Introduction
In this chapter, Skiena has focussed mainly on three fundamental abstract data types: *containers, priority queues and dictionaries*.

### 3.1 Contiguous vs Linked Data Structures
DS can be classified as contiguous or linked, depending upon whether they are based on arrays or pointers.

*Contiguously Allocated Structures*: Arrays, Heaps(created using array), Matrices, and HashTables.
*Linked Data Structures*: Linked List, Trees and Graph Adjacency Lists.

### 3.1.1 Arrays 
Advantages of Array:

* Constant Time Access - Access of any element using index in an array is constant time.
* Space Efficiency - Arrays consist purely of data, so no space is wasted on storing links or other formatting info.
* Memory Locality - Arrays are good for iterating through all elements because all their elements are stored sequentially in a block of contiguous memory. In modern architectures, high speed cache memory is available which is useful while iterating or accessing successive data inside array.

Disadvantages of Array:

* We cannot adjust their size in the middle of the program execution.
* There is a way around this, and that is to use **dynamic arrays**. In dynamic arrays we double its space, each time we run out of space. This involves allocating a new contiguous array and copying the contents of the old array to the newer one and returning the space of old array to storage allocation system.

### 3.1.2 Pointers and Linked Structures
Pointers are important here. It holds linked structures together. We already know how to structure a linked list and a doubly linked list. Further, Skiena has explained the three basic operations on linked list: searching, insertion and deletion.

### 3.1.3 Comparison

LinkedStructures > Array

* Overflow on linked structures can never occur unless the memory is full.
* Insertions and Deletions are simpler.
* With large records(data), moving pointers is easier and faster.

Arrays > LinkedStructures

* Linked Structures require extra space to store pointers.
* Access of an element is not constant time in linked structures.
* Arrays allow better memory locality and cache performance than pointer jumping.


Both Arrays and Linked Structures can be thought of as recursive objects(If we chop some elements from a list, even then we will have a list). This insight leads to simpler list processing, and efficient divide and conquer algos such as quick sort and binary search.

### 3.2 Stacks(LIFO) and Queues(FIFO)

Stacks and Queues can be effectively implemented using either arrays or linked lists. The key issue is whether an upper bound on the size of the container is known in advance, thus permitting the use of a statically allocated array.

### 3.3 Dictionaries

Dictionaries here are an abstract data structure. You should not think it in terms of NSDictionary(of Objective-C) or HashMap. These are implementations of Dictionary and not dictionary as a concept themselves.

The primary objective of dictionary is to find content when we need it.

Primary operations of Dictionary are:

* Search(D, k): given search key *k*, return a pointer to the element in D.
* Insert(D, x): given pointer to element *x*, insert to the set in D.
* Delete(D, x): given pointer to element *x*, remove it from D. 

Some implementations of Dictionary support other useful operations:

* Max/Min(D): Retrieve the item with the largest(or smallest) key. **This enables the dictionary to serve as a priority queue.**
* Predecessor(D, k) or Successor(D, k): Retrieve the item from D who is the predecessor or successor of item with search key *k* in sorted order. This helps us on iterating the dictionary.

---
**Problem:** Use dictionary to remove the duplicate names on mailing list and print names in sorted order

*Solution:*  
> Create an empty dictionary.
> Search in the dictionary, if name does not exist, insert name.
> Use Min(D) operation to start printing names in sorted manner. Use successor(D,x) to traverse the dictionary in sorted order till we get Max(D).

---

**Problem**: What are the asymptotic worst-case running times for each of the seven fundamental dictionary operations (search, insert, delete, successor, predecessor, minimum, and maximum) when the data structure is implemented as: An Unsorted Array and A Sorted Array.

*Solution:*

| Operation | Unsorted Array | Discussion | Sorted Array | Discussion |
|:---------:|:--------------:|:---------: | :------------:| :---------:|
| Search(D, k)| O(n) | Linear search | O(log n) | Binary search|            
| Insert(D, x)| O(1) | at the end of array | O(n) | Binary search to find position and then shift array|
| Delete(D, x)| O(1) | delete and shift last item to deleted position| O(n) | delete, and shift array |
| Successor(D, x)| O(n) | to find successor we would need to traverse | O(1) | D(x+1)|
| Predecessor(D, x)| O(n) | same with predecessor | O(1) | D(x-1) | 
| Minimum(D)| O(n) | linear search |O(1) | D[1]-first element|
| Maximum(D)| O(n)| linear search | O(1) | D[n]-last element |

---

**Problem**: Above problem with Linked lists 
Solution:

|Operation| Singly unsorted | Double Unsorted | Singly Sorted | Double Sorted |
|:-------:|:---------------:|:---------------:|:-------------:|:-------------:|
|Search(D,k)		| O(n) | O(n) | O(n) | O(n) |
|Insert(D,x)		| O(1) | O(1) | O(n) | O(n) |
|Delete(D,x)		| O(n) | O(1) | O(n) | O(1) |
|Successor(D,x)	| O(n) | O(n) | O(1) | O(1) |
|Predecessor(D,x	| O(n) | O(n) | O(n) | O(1) |
|Minimum(D)		| O(n)	| O(n) | O(1) | O(1) |
|Maximum(D)		| O(n) | O(n) | O(1)* | O(1)*|

The maximum element sits at the tail of the list which should normally takes Θ(n) time in singly or double linked list. We can save this time by maintaining the pointer to the tail(for every insertion and deletion). 

The tail pointer can be updated in constant time on doubly linked list. *How?* On insertion, check wether last->next is still NULL, if not then last = last->next. On deletion, check if last == (node to be deleted), if yes, then last = last->prev.

On Singly linked list, this maintenance is a bit complex. Insertion is easy and same as doubly linked list. On deletion, we can save previous node at every step(while traversing), and if node to be deleted is last, then update tail pointer wit prev.

----
##### Skip List

**Is it possible to perform log(n) search on a sorted linked list?**
No, it is not possible to perform a log(n) search on a sorted linked list. 
The answer is to use a different DS known as **Skip Lists**.

In Skip Lists, we create multiple layers so that we can skip nodes. 
See the following example list with 16 nodes and two layers. The upper layer works as an “express lane” which connects only main outer stations, and the lower layer works as a “normal lane” which connects every station. Suppose we want to search for 50, we start from first node of “express lane” and keep moving on “express lane” till we find a node whose next is greater than 50. Once we find such a node (30 is the node in following example) on “express lane”, we move to “normal lane” using pointer from this node, and linearly search for 50 on “normal lane”. In following example, we start from 30 on “normal lane” and with linear search, we find 50.

![](images/SkipList1.png)

**What is the time complexity with two layers?**

n  - no. of nodes
√n - no. of nodes in express lanes.
√n - if we have equally divided the normal lane then there will be √n nodes in every segment of normal lane.
Time Complexity(worst case) : O(√n) + O(√n) = O(√n).
Space Complexity : Θ(√n).

*The time complexity of skip lists can be reduced further by adding more layers.*

---
### 3.4 Binary Search Trees

We have seen DS above that can provide fast search **or** flexible update but not fast search **and** flexible update.
 
A binary search tree is a DS that can provide both fast search and flexible update. A BST is a binary tree(i.e a tree with every node having at most two levels), with a property that every node labeled x, all nodes in the left subtree of x have keys < x while all nodes in the right subtree have keys > x.

```
typedef struct tree {        item_type item; // data item        struct tree *parent;(optional) // pointer to parent        struct tree *left; // pointer to left subtree        struct tree *right; // pointer to right subtree} tree;
```
###3.4.1 Implementing Binary Tree

##### Searching In a Tree
```
tree* search_tree(tree* l, item_type x) {
	if(l == NULL) { return NULL; }
	
	if(l->item == x) { return l; }
	
	return x < l->item ? search_tree(l->left, x) : search_tree(l->right, x);
}
```
Time Complexity: O(h) -- h is height of tree which can be find out in log(n) times, if tree is balanced.

##### Minimum and Maximum Elements in a Tree
Minimum element must be the left most descendent of the root.
```
tree* find_minimum(tree* l) {
	if(l == NULL) {return NULL;}
	
	if (l->left == NULL) { return l; }
	
	return find_minimum(l->left);
}
```

##### Traversal In A Tree
Visiting all the nodes in a rooted binary tree proves to be an important component of many algos. 
*It is a special case of visiting all the nodes and edges in a graph, which will be the foundation of Graph Traversal* 

In-ordered Traversal, Pre-ordered Traversal, Post-ordered Traversal are three different algos for printing out labels of each node. Each traversal algo takes O(n) time.
To print out a tree in a sorted order, we use In-order Traversal.

#####Insertion In a Tree
```
insert_tree(tree* l, item_type x, tree* parent) {
	tree* p; // temp
	if(l == NULL) {
		*p = malloc(sizeof(tree));
		p->item = x;
		p>left = NULL;
		p>right = NULL;
		p->parent = parent;
		l = p;
		return;
	}
	
	x < l->item ? insert_tree(l->left, x, l) : insert_tree(l->right, x, l);
}
``` 
Time Complexity: O(h)

#####Deletion From a Tree
* Deletion is trickier than insertion, because deleting a node means linking back its descendants to the tree.
* Deletion has three basic cases: 
	* Delete a node which has no children.
	* Delete a node which has only one child nodes.
	* Delete a node which has both child nodes. *This one is complex*. 

* The two cases are simple but the third case is complex because we need to find z's(z is node to be deleted) successor y, which lies in z's right subtree and has no left child. 
	* If y is z's right child, then we replace z by y, leaving y's right child alone.
	* If y lies within z's subtree, then we first replace y by its own right child, and then we replace z by y.

Time Complexity: O(h)

###3.4.2 How good are BST?

* When implemented using BST, all three dictionary operations(Search, Insert, Delete) take O(h) time. The smallest height we can hope for occurs when tree is **perfectly balanced**, where h = ⌈log n⌉.
* Lets suppose we are inserting already sorted items in a tree using Insert algo. Because items are already sorted this will result into a binary search tree with only right nodes and thus very similar to linked list, with height = n.
* To solve this problem, we can have input the nodes in a random order. This is good for average case but still does not solve our problem in a very efficient manner.
* To create a BST, where every operation takes O(log n) time, we need to balance our tree after every insertion.
* Sophisticated *balanced* BST has been developed that guarantees the height of the tree always to be O(log n). These data structures are known as red-black trees and splay trees.

---
##### Red-Black Trees

* Skiena has not discussed them in detail. Here is the explanation of Red-Black Trees from **Cormen and other sources**.
* A red-black tree is a BST with one extra bit of storage per node: its colour.
* Following are the red-black tree properties.
	1. Every node is either red or black.
	2. The root is black.
	3. Every NULL node is black.
	4. If a node is red, then both its children are black. There are no two adjacent red nodes (A red node cannot have a red parent or red child).
	5. Every path from root to a NULL node has same number of black nodes. We call this number the black height of red-black tree, bh(n).
* A red-black tree with n internal nodes has height at most 2log(n+1) ~ log(n).
* As with heaps, additions and deletions may destroy the red-black property, so we need to restore it.(Like we use heapify to maintain heap property). 

We restore the properties by changing the colors of some nodes and also by changing the pointer structure. We change the pointer structure through *rotation*.

#####Rotations

Two types of rotations: 
*left rotation:*  swaps the parent node with its right child
*right rotation:*  swaps the parent node with its left child

*left rotation:*
1. assume node x is parent and node y is a non-leaf parent node
2. make y parent and x its left child.
3. but because y already has left child, make this left child as x's right child.

In figure below, x is 5 and y is 7 at the start of rotation.

The pseudo code for Left-Rotation is defined in Cormen.

#####Insertion 
* We can insert a node into an n-node red-black tree in O(n) time.

*To read more about the complete algo, read Cormen.*
Also, **AVL** is one more variant of BST which is balanced.

![](images/rbtrees6.jpg)

---

### 3.5 Priority Queues

* Priority Queues are data structures that provide more flexibility than simple sorting. It is much more cost-effective to insert a new job into a priority queue than to re-sort everything on each such arrival.
* Basic Priority Queue Operations:
	1. Insert(Q, x)
	2. Find-Minimum(Q) or Find-Maximum(Q)
	3. Delete-Minimum(Q) or Delete-Maximum(Q)

---

**Problem:** What is the worst-case time complexity of the three basic priority queue operations (insert, find-minimum, and delete-minimum).
Solution:

| Operation | Unsorted Array | Sorted Array | Balanced Binary Search Tree |
|:---------:|:-------------:|:------------:|:------------------------:|
|Insert(Q,x)| 		 O(1)| O(n) | O(log n) |
|Find-Minimum(Q)|	*O(1)|\*O(1)| \*O(1)   |
|Delete-Minimum(Q)|  O(n)| O(1) | O(log n) |

Find-Minimum(Search) operation in unsorted array takes O(n) time. But we can do it in O(1) for all three DS, by  using an extra variable to store a pointer/index to the minimum entry in each of these structures.
Updating this pointer on each insertion is easy—we update it if and only if the newly inserted value is less than the current minimum.

A very famous priority queue implementation is **Heap**.

---

### 3.7 Hashing and Strings

* Hash Tables are a very practical way to maintain a dictionary.
* They exploit the fact that looking an element in an array takes constant time.
* A hash fn is a mathematical fn that maps keys to integers. 
* The result is unique identifier numbers, but they are so large they will quickly exceed the number of slots in our hash table (denoted by m). Thats why we take mod with m.
* If the table size, m, is selected with enough finesse (ideally m is a large prime not too close to 2^i − 1), the resulting hash values should be fairly uniformly distributed. 

### 3.7.1 Collision Resolution
* **Chaining** is the easiest approach to resolve collision. Represent the hash table as an array of m linked lists. Thus search, insertion, and deletion reduce to the corresponding problem in linked lists.
* Chaining is very simple but devotes a considerable amount of memory to pointers.
* **Open Addressing** is an alternative to Chaining. The hash table is maintained as an array of elements (not buckets), each initialised to null.
	* Insertion: On an insertion we check if the desired position is empty, if yes, then insert at that position. If not, we search linearly the next empty slot.
	* Search: Go to appropriate hash value. If not found, then search linearly.
	* Deletion: This is complex since removing one element might break a chain of insertions.
	
	>Assume hash(x) = hash(y) = hash(z) = i. And assume x was inserted first, then y 	and then z. 
	> In open addressing: table[i] = x, table[i+1] = y, table[i+2] = z.
	> Now, assume you want to delete x, and set it back to NULL.	> When later you will search for z, you will find that hash(z) = i and table[i] = NULL, and you will return a wrong answer: z is not in the table.
	> To overcome this, you need to set table[i] with a special marker indicating to the search function to keep looking at index i+1, because there might be element there which its hash is also i.
	
|		|Hash Table(Expected)| Hash Table(Worst Case)|
|:----:|:-----:|:------:|
| Search(L, k) | O(n/m) | O(n) |
| Insert(L, x) | O(1) | O(1) | 
| Delete(L, x) | O(1) | O(1) |
| Successor(L, x) | O(n+m) | O(n+m) |
| Predecessor(L, x) | O(n+m) | O(n+m) |
| Minimum(L) | O(n+m) | O(n+m) |
| Maximum(L) | O(n+m) | O(n+m) |

*A hash table is pragmatically the best DS to maintain a dictionary.*

### 3.7.2 Efficient String Matching via Hashing

* The primary DS for a string is an array of characters.
* The most fundamental operation on text strings is substring search(pattern matching).
* There are algos for pattern matching, one such algo is **Boyer-Moore Algo**(it is in Raywenderlich's Swift Algorithm Club, repo in github). The time complexity for Boyer-Moore algo for no pattern found is O(n+m) where n is length of strings and m is length of pattern) and if string is found at every character then O(nm). Space complexity is O(m) for the hash map. 
* Skiena has discussed *hashing-based linear expected-time algo* known as **Rabin-Karp algorithm**.
	* The simplest algo for String matching is this naive algo. Slide the pattern one by one on the given text and checks characters at the current shift. Worst case is O(nm).
	* Like the Naive algo, Rabin-Karp also slides the pattern but unlike naive, it matches the hash value of the pattern with the hash value of current substring of text, and if hash value matches then only it starts matching individual characters.
	* This means the algo needs hash value of 
		1. pattern
		2. every substring of m length
	* The complexity is that an immature hash function will take O(m) time itself making the algo complexity effectively O(mn).
	* It means we need a hash function with this property. Hash at the next shift must be efficiently computable from the current hash value and next character in text.
	* The hash function suggested by Rabin is known as **Rabin fingerprint** and calculates an integer value. The Rabin fingerprint treats every substring as a number in some base, the base being usually a large prime.
	
	>For example, if we have text "abracadabra" and we are searching for a pattern of length 3, the hash of the first substring, "abr", using 101 as base is:
	>
	> // ASCII a = 97, b = 98, r = 114. 
hash("abr") = (97 × 101<sup>2</sup>) + (98 × 101<sup>1</sup>) + (114 × 101<sup>0</sup>) = 999,509 

	>//             base   old hash    old 'a'         new 'a'
hash("bra") = [101<sup>1</sup> × (999,509 - (97 × 101<sup>2</sup>))] + (97 × 101<sup>0</sup>) = 1,011,309

### 3.7.3 Duplicate Detection via Hashing

The key idea of hashing is to represent a large object (be it a key, a string, or a substring) using a single number.

Hashing has a variety of clever applications beyond just speeding up search.

Consider following problems with nice hashing solutions:

* ==*Is a given document different from all the rest in a large corpus?*== - A search engine with a huge database of n documents spiders yet another webpage D. How can it tell wether this webpage is a new one or a duplicate?

	Use Hashing to hash D to an integer and compare it to hash codes of the rest of the corpus. Only when there is a collision there is a chance of duplicity.
	
* ==*Is part of this document plagiarised from a document in a large corpus?*== - – A lazy student copies a portion of a Web document into their term paper. “The Web is a big place,” he smirks. “How will anyone ever find which one?”

	We could build hash-table of all overlapping substrings of length w in all the documents in the corpus. Whenever there is a match, there is likely a common substring of length *w* between the two documents. We should choose *w* large enough. Downside is that size of hash-table will be huge. 
	
* ==*How can I convince you that a file has not changed?*== - In a closed-bid auction, each party submits their bid in secret before the announced deadline. If you knew what the other parties were bidding, you could arrange to bid $1 more than the highest opponent and walk off with the prize as cheaply as possible. Thus the “right” auction strategy is to hack into the computer containing the bids just prior to the deadline, read the bids, and then magically emerge the winner.

	 What if everyone submits a hash code of their actual bid prior to the deadline, and then submits the full bid after the dead- line? The auctioneer will pick the largest full bid, but checks to make sure the hash code matches that submitted prior to the deadline.
	  
### 3.8 Specialised Data Structures

All DS discussed so far are well known. There are other DS which are not very well known and gets used to represent points in space, strings and graphs.

The design principles of these DS are same i.e. they all support some basic operations.

* **String DS**: *Suffix trees/arrays* are special DS that preprocesses strings for faster pattern matching
* **Geometric DS**: *kd-trees*
* **Graph DS**: *adjacency matrices, adjacency lists*
* **Set DS**: *bit vectors*, *Union-find DS*

## 3.10 Exercises

*Stacks, Queues and Lists*

**3-1.** [3] A common problem for compilers and text editors is determining whether the parentheses in a string are balanced and properly nested. For example, the string ((())())() contains properly nested pairs of parentheses, which the strings )()( and ()) do not. Give an algorithm that returns true if a string contains properly nested and balanced parentheses, and false if otherwise. For full credit, identify the position of the first offending parenthesis if the string is not properly nested and balanced.

*Sol:*
> Use stack to solve this problem.

**3-2.** [3] Write a program to reverse the direction of a given singly-linked list. In other words, after the reversal all pointers should now point backwards. Your algorithm should take linear time.

*Sol:*

```
typedef struct Node {
	item_type data;
	struct Node* next;
} Node;

Node* reverse(Node* head) {
	
	if(head == NULL || head-> next == NULL) {return head}; 

	Node* curr = head;
	Node* prev = NULL;	
	Node* next = head;
	
	while(curr != NULL) {
		next = curr -> next;
		curr->next = prev;
		
		prev = curr;
		curr = next;
	}
	
	return prev;
}

```
---

*Trees and Other Dictionary Structures*

**3-4.** [3] Design a dictionary data structure in which search, insertion, and deletion can all be processed in O(1) time in the worst case. You may assume the set elements are integers drawn from a finite set 1, 2, .., n, and initialisation can take O(n) time.
 
*Sol:*

> 1. Initialise an array of count *n*, and save 0 at every index. 
> 2. For every integer *k*, index in Dictionary is *k-1*. At every insert, change value saved to 1. This is O(1).
> 3. On delete(k), change the saved value at index *k-1* to 0. This is O(1).
> 4. On search(k), find element at index *k-1*, if 1 then found ow not found.
> 
> This is also an example of **bit array**.  


**3-6.** Describe how to modify any balanced tree data structure such that search, insert, delete, minimum, and maximum still take O(log n) time each, but successor and predecessor now take O(1) time each. Which operations have to be modified to support this?

*Sol:*

> 1. Keep pointers to successor and predecessor in Tree structure.
> 2. Modify insert and delete operations to update values in these pointers.


**3-7.** Suppose you have access to a balanced dictionary data structure, which supports each of the operations search, insert, delete, minimum, maximum, successor, and predecessor in O(log n) time. Explain how to modify the insert and delete operations so they still take O(log n) but now minimum and maximum take O(1) time. (Hint: think in terms of using the abstract dictionary operations, instead of mucking about with pointers and the like.)

*Sol:*

> 1. Maintain two variables for minimum and maximum.
> 2. At every insert, update these values by simply comparing the inserted value with min/max.
> 3. At every delete, if deleted variable == min/max, call successor/predecessor, and update the mi/max variables.


**3-9.** [8] A concatenate operation takes two sets S1 and S2, where every key in S1 is smaller than any key in S2, and merges them together. Give an algorithm to concatenate two binary search trees into one binary search tree. The worst-case running time should be O(h), where h is the maximal height of the two trees.

*Sol:*

> Since all keys of S1 is less than S2.
> 
> I first thought of putting S2 on right of last right most node of S1 and then use rotation to balance the tree. This approach though should work but is unnecessarily complex.
> 
> Correct approach should be:
> 
> 1. We can have S1 as left subtree and S2 as right subtree
> 2. At The root of such concatenation, should be a node with data item grater than max of S1 and less than min of S2.
> 3. We can get the min of S2 and make it root node by updating its left and right pointers and setting its parent as NULL. Also before setting its parent NULL, point parent->left = NULL.

---

*Interview Problems*

**3-18.** [3] What method would you use to look up a word in a dictionary?

*Sol:* 
> Binary Search

**3-19.** [3] Imagine you have a closet full of shirts. What can you do to organise your shirtsfor easy retrieval?

*Sol:*
>  Hash it. Take every shirt and put it in a different drawer depending on the color of your shirt. There we can maintain a Queue like structure to select different shirts daily.

**3-20.** Write a function to find the middle node of a singly-linked list.

*Sol:* 
> Use two pointers, tortoise and hare. Tortoise will step one place, hare will move two places. When hare will reach end of linked list, tortoise should be at the middle.

**3-21.** Write a function to compare whether two binary trees are identical. Identical trees have the same key value at each position and the same structure.

*Sol:*

```
bool compare(Tree* t1, Tree* t2) {
	if(t1 == NULL && t2 == NULL) {return true};
	
	if(t1 != NULL && t2 != NULL) {
		return ((t1->data == t2->data) && compare(t1->left, t2->left) && compare(t1->right, t2->right);
	}
	
	return false;
}

```
**3-22.** Write a program to convert a binary search tree into a linked list.

*Sol:*
> Use In-order traversal to read all nodes and push their data in a LIFO queue.
> Read queue to create a linked list.
> Space Complexity:O(n) and Time Complexity:O(n)

Other solution is to use Left-shift rotation repeatedly on all left nodes.
> Left-shift rotation on left subtree of root.
> Then traverse right subtree and left-shift all left nodes as well.
> Space Complexity:O(1) and Time Complexity:O(n)

**3-24.** What is the best data structure for maintaining URLs that have been visited by a Web crawler? Give an algorithm to test whether a given URL has already been visited, optimising both space and time.

*Sol:*
> An NSSet like DS would be ideal for this case. NSSet contains method is O(1) and can be used to know wether a given URL has already been visited. NSSet uses hashing internally.

**3-25.** You are given a search string and a magazine. You seek to generate all the characters in search string by cutting them out from the magazine. Give an algorithm to efficiently determine whether the magazine contains all the letters in the search string.

*Sol.*
> * Create a Counted Set for every character of search string. 
> * Traverse every character of magazine. If that character is in Set, decrement its count by removing it from Set(when count becomes 0, Counted Set removes the character).
> 
> If Set is empty, we are done. If we reach the end of magazine, and set is non-empty, it means Set magazine does not contain all characters.


**3-26.** Reverse the words in a sentence---i.e., My name is Chris becomes Chris is name My. Optimise for time and space.

*Sol*:
> * Reverse complete string. `sirhC si eman yM.`
> * Reverse each word.`Chris is name My`
> 
> Time Complexity: O(n), Space: O(1)

**3-27.** Determine whether a linked list contains a loop as quickly as possible without using any extra storage. Also, identify the location of the loop.

*Sol:*

```
Node* detectLoopNodeInLinkedList(Node* head) {
	if(head == NULL || head->next == NULL) {return NULL}; // No loop is possible
	
	Node* slow = head;
	Node* fast = head->next;
	
	while(fast != NULL) {
		slow = slow->next;
		
		if(fast->next == NULL) { return NULL; }
		fast = fast->next;
		
		if(slow == fast) {break;}
	}
	
	if(slow == fast) {
		// this means e have detected a loop and only thing left to find is the location of the loop.
		
		slow = head;
		while(slow != fast) {
			slow = slow->next; /// move slow and fast one step 
			fast = fast->next;
		}
		return slow;
	}
	
	return NULL;
}

```
Why does it work?

![](\images/loop_linkedlist.png)
> length of loop = y+z
> distance before loop = x
> when slow and fast meet, slow distance = x+y, fast distance = x+y+z+y = 2*d(fast was going twice of slow)
> 2d = x+2y+z
> 2x+2y = x+2y+z
> x = z

This shows after getting meeting point, if one pointer is again moved at the beginning and both move one step, then they will meet at the start of the loop.

**3-28.** You have an unordered array X of n integers. Find the array M containing n elements where M<sub>i</sub> is the product of all integers in X except for X<sub>i</sub>. You may not use division. You can use extra memory. Hint: There are solutions faster than O(n<sup>2</sup>).

*Sol:*

>	X = `1|2|3|4`, 			n = 4
>	M = `24|12|8|6`
>
> 	We can use two arrays left and right.

```
left = initialise array of count n with all values 1. 
right = initialise array of count n with all values 1. 
 
for(i = 1; i < n; i++) {
	left[i] = left[i-1] * X[i-1];
}

for(i = n-1; i>=0; i--) {
	right[i] = right[i+1] * X[i+1];
}

for(i = 0; i < n; i++) {
	M[i] = left[i] * right[i];
}

```

**3-29** [6] Give an algorithm for finding an ordered word pair (e.g., “New York”) occurring with the greatest frequency in a given webpage. Which data structures would you use? Optimise both time and space.

*Sol:*

First thing in my mind is to use HashMap
> * to save word as a key and frequency as a value. 
> * Simultaneously, maintain a max frequency variable, which can be checked after every insertion and updated accordingly.
> * Time: O(n)--iteration | Space: O(n)--HashMap

My second guess was to use a priority queue, created using Balanced Binary Search Tree but that would make insert and delete O(log n) and will make insertion O(n log n) which is intact not better than hash map.

I read on internet about this problem. Time complexity is going to be O(n) in any case because we would have to read all the words inside the magazine.
We could though try to reduce space complexity using **trie.** 

---

#### Trie

*Why is a Trie needed?*

* Trie is also known as prefix tree(maybe because it can be used to find prefixes) and is used to store associative data structures
* Tries are primarily used to index and search strings inside a text.
* Let word be a single string and let dictionary be a large set of words. If we need to know that a single word is inside the dictionary, the tries are a data structure that can help us. But you may be asking yourself, “Why use tries if set <string> and hash tables can do the same?” There are two main reasons:
	1. The tries can insert and find strings in O(l) times where l is the length of the word. This is much faster than Set but not hash table.
	2. The set<string> and the hashtables can find in a dictionary words that match exactly same with the single word whereas a trie allows us to find words that have a single character different, a prefix in common, a character missing etc.
	3. Unlike a hash map, a Trie does not need to worry about key collisions
	4. Trie structures can be alphabetically ordered by default.
	
*Application in Real World*

* Web browser can auto complete text or show many possibilities of the text by using a trie. 

*What is a trie?*

The trie is an infix of the word **retrieval** because it can find a single word in a dictionary by only using its prefix.

*I found a medium [article](https://medium.com/algorithms/trie-prefix-tree-algorithm-ee7ab3fe3413), which helped me immensely in understanding trie.*

* A trie stores data in "steps". Each step is a node in the trie.
* Storing words is a perfect use case for this kind of tree, since there are a finite amount of letters that can be put together to make a string.
* We can see this below example of a trie out of directories. This is a trie that contains two words: apple and app, where $ denotes the end of the word.

![](images/trie.gif)

* If, we were to add "aperture", we would loop over the letters of the word "aperture" in the trie and if letter found, then step into it, if letter not found as a child of the current node, then create it and then step into it.
![](images/trie2.gif)

*How to create Trie?*

![](images/trie3.png)

This image and following code I have picked from Raywenderlich algo [repo](https://github.com/raywenderlich/swift-algorithm-club/tree/master/Trie).

**Insertion**
Insertion into a Trie requires you to walk over the nodes until you either halt on the node that must be marked as terminating or reach a point where you need to add extra nodes

```
func insert(word:String) {
	guard !word.isEmpty else {
		return 
	}
	
	var currentNode = root 
	
	for character in word.lowercased().characters {
		if let childNode = currentNode.children[character] { // move current node 
			currentNode = childNode
		} else { // node not found
			currentNode.add(value:character)
			currentNode = currentNode.children[character]! 
		}
	}
	
	// if the word was already present then 
	if(current.isTerminating) {
		return 
	} else {
		wordCount += 1// this means we have added a new word
		currentNode.isTerminating = true
	}
}

```

**Deletion**

Deletion is more tricky because nodes in trie may be shared between different words. Consider the two words "Apple" and "App". Inside a Trie, the chain of nodes representing "App" is shared with "Apple". 

```
func remove(word:String) {
	guard !word.isEmpty else {
		return
	}
	
	// 1
	guard let terminalNode = findTerminalNodeOf(word:word) else {
		return
	}
	
	if terminalNode.isLeaf { // trie node is leaf when it has no children
		deleteNodesForWordEndingWith(word:word)
	} else { // if node has children then 
		terminalNode.isTerminating = false
	}
	
	wordCount = -1
}

private func findLastNodeOf(word: String) -> Node? {
      var currentNode = root
      for character in word.lowercased().characters {
          guard let childNode = currentNode.children[character] else {
              return nil
          }
          currentNode = childNode
      }
      return currentNode
  }

private func findTerminalNodeOf(word: String) -> Node? {
    if let lastNode = findLastNodeOf(word: word) {
        return lastNode.isTerminating ? lastNode : nil
    }
    return nil
}

private func deleteNodesForWordEndingWith(terminalNode: Node) {
    var lastNode = terminalNode
    var character = lastNode.value
    while lastNode.isLeaf, let parentNode = lastNode.parentNode {
      lastNode = parentNode
      lastNode.children[character!] = nil
      character = lastNode.value
      if lastNode.isTerminating {
        break
      }
    }
  }

```
1. findTerminalNodeOf traverses through the Trie to find the last node that represents the word. If it is unable to traverse through the chain of characters, it returns nil.
2. deleteNodesForWordEndingWith traverse backwords, deleting the nodes represented by the word.


---



