# Sorting and Searching

### Applications Of Sorting
* But the punch line is this: clever sorting algorithm exists that run in O(n log n).
* An important *algorithm design* technique is to use sorting as a basic building block, because many problems become easy once a set of items is sorted.
* Consider the following applications:
	* *Searching*: Binary Search in O(log n) time if items are sorted
	* *Closest Pair*: Given a set of n numbers, find a pair that have the smallest difference between them? If items are sorted, this problem can be solved in O(n) time.
	* *Element Uniqueness*: Are there any duplicates in a set of n items? Again sorting the items first, helps immensely.
	* *Frequency Distribution*: Given a set of n items, which element occurs largest number of times? If items are sorted, we can sweep from left to right and count them. To find out how an arbitrary element k occurs, use binary search to find the k in a sorted set. By walking to the left of this point until the item is not k and similarly walking to the right, we can find out the number of times k occur, in O(log n c) times where c is the number of occurrences of k. Even better, we can further use Binary search in recursion to find left most and right most index to complete this problem in log n.
	* *Selection*: What is the kth largest element in an array? If elements are sorted in descending order, we can find the kth largest element at index *k*. The complexity is O(nlogn) of course. **Though, we can reduce this complexity further to O(n + klogn) by using Min/Max heap**. Best way to achieve this is to use quick sort algo but stop when kth smallest element is found as pivot. This has linear time complexity for average case.
	* *Convex Hulls*: I encountered convex hulls at Introduction To Algorithms(Cormen) as well. The convex hull is like a rubber band stretched over points in the plane and then released. Sorting can help in constructing the convex hull. 

____

##### Stop And Think : Finding the intersection

**Problem:** Give an efficient algorithm to determine whether two sets (of size m and n, respectively) are disjoint. Analyse the worst-case complexity in terms of m and n, considering the case where m is substantially smaller than n.

*Sol:*

The first algorithm that comes to my mind is :

__Algo 1__:
> 1. Sort first and second sets
> 2. Use merge like process to compare elements

```
func isDisjointSets(s1:Set, m:Int, s2:Set, n:Int) -> Bool {
	s1.sort(<) ; // sort first
	s2.sort(<) ; // sort second
	
	var mi = 0;
	var ni = 0;
	
	while(mi < m && ni < n) {
		if s1[mi] < s2[ni]  {
			mi = mi+1;
		} else if s2[ni] < s1[ni] {
			ni = ni+1;
		} else {
			return false 
		}
	}
	return true
}

```
> Complexity for this Algo is : O(mlogm + nlogn + m + n)
 
 
 The other algo sorts the smaller set and then uses binary search to find all elements of set2.

 __Algo2__:
> 1. Sort smaller Set 
> 2. Iterate through every element of larger set and use Binary Search to find element.
> Complexity for this Algo is : O(mlogm + nlogm), which is slightly better than the Algo1.


__Algo3__:
> 1. Create HashMap with keys equal to every element of smaller set an their value set as 1.
> 2. Iterate through larger set and check whether the elements of larger set are already in HashMap(if their value is 1).
> Complexity: Time - O(n), Space:O(m)

```
func isDisjointSets(s1:Set, m:Int, s2:Set, n:Int) -> Bool {
	var hashMap:[Int:Int] = [:]
	for i in 0..<m {
		hashMap[s[i]] = 1; // Insert element in Dictionary
	}
	
	for i in 0..<n {
		let element = s2[i];
		guard let value = hashMap[element] else {
			continue
		}
		return false // element found inside hash-map
	}
	return true;
} 

```
 ____
 
### 4.2 Pragmatics Of Sorting

* *What Should we do with equal keys?* Sometimes it is required to leave the items in the same relative order as in original permutation. The sorting algorithms that can achieve this are called **stable**. Unfortunately few fast algos are stable.
* *What about non-numerical data?* How do we sort data where we can not simply use less than(<) or greater than(>) operations? Like we have seen in Objective-C sorting, we need to provide application specific pairwise-element comparison function. 

### 4.3 HeapSort: Fast Sorting via Data Structures

######Selection Sort

> 1. Iterate elements from 0 <= i < n  
> 2. Linear sweep through unsorted portion of the array to find minimum element. If a minimum element is found, then swap before moving forward.

```
func selectionSort(array:Array<Int>, n:Int) {
	for i in 0..<n { // Iterate elements
		var min = array[i];
		for j in i+1..<n {
			if array[j] < min { swap(&array[i], &array[j]) }
		}
	}
}

```
> Complexity: O(n<sup>2</sup>)

It takes O(n) time to find the smallest item. In a priority queue, it takes O(log n) time to find smallest element. We can use such a priority queue to speed up selection sort from O(n<sup>2</sup>) to O(nlogn).
This sorting algorithm is known as **HeapSort**. Heapsort is nothing but an implementation of selection sort using heaps.

### 4.3.1 Heaps

* Heap is a DS that is used a priority queue as it efficiently supports priority queue operations, find-min/max, extract min/max, insert and delete.
* Heaps can be implemented using arrays, making it fast to access elements from memory.

---

*Before going further, lets revise Heap(studied earlier in Corman)*

To maintain heap below algos and understanding are required:

* In Max Heap, the value of the node is equal to or less than its parent. A[Parent(i)] >= A[Left(i)]
* The Binary Heap DS is an array, which we can view as **complete binary tree** with 
	* Parent(i) = [i/2] // shift i right one bit position
	* Left(i) = 2i // shift i left one bit position
	* Right(i) = 2i+1; // shift left one bit position and add one as low-order bit.

###### MAX-HEAPIFY

Precondition for MAX-Heapify is that left and right subtrees are max heaps in their own right.

```
func maxHeapify(a:Array<Int>, i:Int) {
	
	let left = getLeft(i);
	let right = getRight(i);
	
	let highest = i;
	
	if left > highest { highest = left }
	
	if right > highest{ highest = right }
	
	if i != highest { // Needs reordering
		temp = i;
		i = highest;
		highest = temp;
		maxHeapify(a,highest)
	}  
}

```
Complexity: O(lg n)

###### BUILD MAX HEAP

We iterate elements from `n/2 to 1` while building the heap because all elements at indexes `n/2 to n` are leaves and hence satisfy heap property automatically(as they don't have children of their own) and does not require to max-heapify them. It also means, the order of these elements among each other are not defined.

```
func buildMaxHeap(a:Array<Int>) {
	for i in (0..<a.count/2).reversed() { // reverse array iteration
		maxHeapify(a, i)
	}
}

```
**Complexity:**
The complexity of build algo is interesting. On paper, it looks time complexity is O(nlgn), where O(lgn) is each call to MaxHeapify and O(n/2) ~ O(n) times we make that call but in practice the build algo has a tight bound of O(n). How?

Note only the last insertion(at index 0) will actually take ⌊lg n⌋ steps. The maxHeapify has time complexity of O(h) but most of these heaps are extremely small. This makes the complexity coverage to linear.


####### Extract-Max

```
func heapExtractMax(a:Heap<Int>) -> Int {
	if(a.heapSize < 1) { print("heap underflow") }
	let max = a.first // This should be highest in a maxheap
	a.first = a[a.heapSize]
	a.heapSize = a.heapSize - 1
	maxHeapify(a, 0)
	return max
}
```
Complexity: O(lg n)

###### HeapSort

```
func heapSort(a:Array<Int>) {
	buildMaxHeap(a) // Build MaxHeap
	
	for i in (1..<a.count).reversed() { // Reverse array iteration
	/*	let lastObject = a[i]
		let firstObject = a.first // This should be highest in a maxheap
		
		a[i] = firstObject // Last element is at its correct position now
		a.heapSize = a.heapSize - 1  // Hence decrease heapSize

		a[0] = lastObject // Swapping last object
		
		maxHeapify(a, 0) // Run Max-Heapify  at first position */
		
		a[i] = heapExtractMax(a)
	}
	
}

```
Complexity : O(nlgn)


###### Heap Increase Key

```
func heapIncreaseKey(a:Array<Int>, i:Int, key:Int) {
	if key < a[i] { print("new key is smaller than i") return}
	
	a[i] = key
	while i > 1 && a[parent(i)] < a[i] {
		let temp = a[parent(i)]
		a[parent(i)] = a[i]
		a[i] = temp
		
		i = parent(i)
	}
}

```
Complexity: O(lg n)

###### Insert

Heap Increase key algo can be used to insert new element in heap

```
func insertAt(a:Array<Int>, key:Int) {
	a.heapSize = a.heapSize + 1 
	a[a.heapSize] = INT_MINIMUM
	heapIncreaseKey(a, a.heapSize, key)
}

```
Complexity: O(lg n)

heapsize property here is an important property. Its obviously not defined in an array hence we can have a struct defining heap

```
struct<T> {
	var heapSize:Int;
	var a:Array<T>
} Heap;

```
---

Ok, So this was our revision from Corman. Lets get back to what Skiena Sir is saying about Heaps.

**We have seen in heap implementation that we can store any binary trees in an array without pointers. Then why don't we use arrays for binary search trees?**

???

---

**Problem:** How can we efficiently search for a particular key in a heap?
*Sol:* We don't know much about elements saved at indexes n/2 to n.(Because in a heap those elements are at last level and are all leaves and hence their order is not defined). Thus we don't have any other option than to search linearly.


----

**Problem:** Given an array-based heap on n elements and a real number x, efficiently determine whether the kth smallest element in the heap is greater than or equal to x. Your algorithm should be O(k) in the worst-case, independent of the size of the heap. Hint: you do not have to find the kth smallest element; you need only determine its relationship to x.

*Sol:*
Simple solution was to run extract min k times but that would mean complexity of O(klgn).

An O(k) solution will traverse all nodes which are less than x and if counter is greater than k.

```
int heap_compare(priority_queue *q, int i, int count, int x) {	if (count <= 0) || (i > q->n) return(count);	
	if (q->q[i] < x) {    	count = heap_compare(q, Left(i), count-1, x);    	count = heap_compare(q, Right(i), count, x);	}	return(count);
}

```

---

### 4.5 Merge Sort

Lets revive merge sort from memory.

* Merge sort is based on divide-and-conquer paradigm of algorithm design
* It runs in O(nlogn) time with extra space.
* It is a stable sort.

```

func merge(inout a:Array<Int>, low:Int, mid:Int, high:Int) {
	let leftArray =  Array(a[low..<mid+1])
	let rightArray = Array(a[mid+1..<high+1])
	
	let lCount = leftArray.count
	let rCount = rightArray.count
	
	var index = low;
	var lIndex = 0;
	var rIndex = 0;
	
	while ((index-low) < lCount+rCount) {
		if(lIndex >= lCount) { break }
		if(rIndex >= rCount) { break }
		
		if(leftArray[lIndex] <= rightArray[rIndex]) {
			a[index] = leftArray[lIndex]
			index = index+1
			lIndex = lIndex+1
		} elseif(leftArray[lIndex] > rightArray[rIndex]) {
			a[index] = rightArray[rIndex]
			index = index+1
			rIndex = rIndex +1
		}
	}
	
	if(lIndex < lCount) {
		a[index] = leftArray[lIndex]
		index = index+1
		lIndex = lIndex+1
	}
	
	if(rIndex < rCount) {
		a[index] = rightArray[rIndex]
		index = index+1
		rIndex = rIndex +1
	}
}

func mergeSortHelper(inout a:Array<Int>, low:Int, high:Int) {
	if (low >= high) { return } 
	var mid:Int = low + (high-low)/2
	mergeSortHelper(a, low, mid)
	mergeSortHelper(a, mid+1, high)
	merge(a, low, mid, high)
}

func mergeSort(a:Array<Int>) {
	mergeSortHelper(a, 0, a.count-1)
}

```

The primary disadvantage of Merge Sort is the need to create arrays while merge subroutine.
**But merge sort is a great algorithm for sorting linked lists**, because it is easy to merge two sorted linked lists without needing extra space, by just rearranging pointers.


### 4.6 Quicksort

* Quicksort randomly selects a *pivot* p and separates the rest of the n-1 items into three piles. First pile is of elements less than pivot, second is of elements greater than or equal to pivot and third one is still unexplored.
* Such partitioning means pivot ends up at its correct position in sorted order and we can recursively sort the left and right piles.

```
func partition(inout a:Array<Int>, low:Int, high:Int) {
	var p = high   // Pivot
	var firstHigh = low  // Divider position for pivot
	
	for(i in low..<high) {
		if a[i] < a[p] { 
			swap(&a[i], &a[firstHigh]) 
			firstHigh = firstHigh+1	
		}
	}
	swap(&a[p], &a[firstHigh])
	return firstHigh
}

func quickSortHelper(inout a:Array<Int>, low:Int, high:Int) {
	var pivot:Int
	if(low < high) {
		pivot = partition(a, low, high)
		quickSortHelper(a, low, p-1)
		quickSortHelper(a, p+1, high)
	} 
	return
}

func quickSort(a:Array<Int>) {
	quickSortHelper(a, 0, a.count-1)
}

```

Complexity:

* The complexity of quicksort depends on the position of pivot element in sorted list. If we consistently get unlucky(we either choose smallest or largest element as pivot) and our pivot element always splits the array as unequally as possible, that is we are always left with subproblem of size n-1(instead of n/2), then the complexity is Θ(n^2 ).
* To make quick sort quicker, we need a pivot element that is close to the centre of the sorted array(like in merge sort). The best pivot is exactly the median but unfortunately, we only have a 1/n chance of selecting that pivot.
* On average, random quicksort partition trees are very good and thus on average quick sort has complexity Θ(nlogn)

---

#######Stop and Think: Nuts and Bolts

**Problem:**  You are given a collection of n bolts of different widths, and n corresponding nuts. You can test whether a given nut and bolt fit together, from which you learn whether the nut is too large, too small, or an exact match for the bolt. The differences in size between pairs of nuts or bolts are too small to see by eye, so you cannot compare the sizes of two nuts or two bolts directly. You are to match each bolt to each nut.Give an O(n^2 ) algorithm to solve the nuts and bolts problem. Then give a randomised O(n log n) expected time algorithm for the same problem.

*Sol:*
1. The brute force approach of picking up a bolt and then checking all nuts for that bolt, will give O(n^2 ) algo.
2. If we can sort the nuts and bolts by size, we would have a matching, since ith largest nut should match ith largest bolt but the problem is we cant compare bolt or nut sizes by comparing among each other or by any property and we need to compare bolt with nut for any kind of partitioning.
3. We can do one thing. We can randomly select a bolt b, and then partitions the nuts into those of sizes less than b and sizes greater than b and we can find the matching nut as well and then use this nut to further partition the bolts. Now, as we have used quicksort recursively, we can use this algo to find the pairs in O(n logn).

--- 

**Sorting can be used to illustrate most algorithm design paradigms. 
Data structure techniques(HeapSort), divide-and-conquer(MergeSort), randomisation(QuickSort), and incremental construction(BucketSort) all lead to efficient sorting algorithms.**

All sorting algos discussed so far are known as *Comparison Sorts* because the sorted order they determined is based only on comparison b/w the input elements and has worst-case or average case time complexity as O(nlogn).

### 4.7 Distribution Sort: Sorting via Bucketing

Lets revise Bucket Sort from Cormen. 

* Bucket Sort sorts in O(n) time for average case but like in *Counting Sort* (another linear sort if input is known to be integers in a small range), it assumes something about the input.

**Algo**
> BucketSort(a:Array<Float>, n:Int)
> 1. Create n empty buckets
> 2. Traverse all elements of array and Insert a[i] into appropriate bucket
> 3. Sort individual buckets using insertion sort // this is important
> 4. Concatenate all sorted buckets to create a sorted array

Complexity: 
If we analyse above algo, steps 1, 2 and 4, should take O(n) time. The step to analyse is Step 3 where we are doing insertion sort to sort buckets.
We know insertion sort is O(n^2 ) but insertion sort is fast when there are few items to sort. 
*The condition for bucket sort to run in O(n) time is that input is uniformly distributed*.

### 4.9.3 Square and Other roots

We can use Binary Search to find square root of number n.
For n > 1, square root can not be greater than n/2(if we are talking in pure integers). It implies we should start with low = 0 and high = n/2 and do binary search to get the square root.

### 4.10 Divide and Conquer

* Two important algorithm design paradigms are based on dividing the problem into smaller problems. Smaller problems are less overwhelming, and they permit us to focus on details that are lost when we are studying the entire problem. 
* *Dynamic Programming* is one such paradigm where we remove one element from the problem, solves the smaller problem and then uses solution to this smaller problem to add back the element in the proper way.
* *Divide and Conquer* splits the problem in halves, solves each half and then stitches back together to form a solution.

Divide and Conquer is used in algorithms such as Merge Sort, the fast Fourier Transform and Strasses's Matrix Multiplication algo. 

* Divide and Conquer is used in Binary Search and it is easy to fathom there but it is difficult to apply in practice for other cases, because it often involves recursion and asymptotic of recurrence relations.
* The time complexities of Divide and Conquer algorithms are difficult to understand and prove. Skiena has tried to explain them but it was difficult to understand. Leaving that section for now.

---
### Shell Sort 
* It is an improved version of insertion sort
* Starts by comparing elements far apart, then elements less far apart. The interval can be selected accordingly. There are numerous famous sequences with Shell himself using N/2, N/4,...1
* Finally we compare adjacent elements just like insertion sort but by this stage the elements are sufficiently sorted that the final stage running time is closer to O(n) than to O(n^2 ).

![](images/shell_sort.jpg)

**Increment Sequences**

1. Shell's original sequence: N/2, N/4,..1
2. Hibbard's increments: 1, 3, 7..2^k -1 
3. Knuth's increment: 1, 4, 13,..(3^k -1)/2 -- (i = 3i+1)


**Algorithm**

1. Initialise the value of interval using any sequence
2. Divide the list into smaller sublist of equal interval h
3. Sort these sub-lists using insertion sort
4. Repeat until complete list is sorted

---
### Radix Sort

* Radix Sort is digit by digit sort starting from least significant bit to most significant bit.
* Radix sort uses counting sort as its subroutine. (Counting sort is stable as well)
* Radix sort is one of the fastest sorting algo for numbers or strings of letters

Example:

170, 45, 75, 90, 802, 24, 2, 66

1. Sort by 1s place: 170, 90, 802, 2, 24, 45, 75, 66
2. Sort by 10's place: 802, 2, 24, 45, 66, 170, 75, 90
3. Sort by 100's place: 2, 24, 45, 66, 75, 90, 170, 802

---

### 4.11 Exercises

*Applications of Sorting*

**4-1.** The Grinch is given the job of partitioning 2n players into two teams of n players each. Each player has a numerical rating that measures how good he/she is at the game. He seeks to divide the players as unfairly as possible, so as to create the biggest possible talent imbalance between team A and team B. Show how the Grinch can do the job in O(n log n) time.

*Sol:*
Algo 1:
>Use any sorting algo to sort the team in exceeding order of numerical rating. Then divide the team in two halves. Complexity: O(nlogn)

Algo2: 
> Use pivot as median to divide the team in two halves. Complexity: O(n) because we don't need to sort the two halves, we just need to divide it with median.

**4-2.** For each of the following problems, give an algorithm that finds the desired numbers within the given amount of time
(a) Let S be an unsorted array of n integers. Give an algorithm that finds the pair x, y ∈ S that maximises |x − y|. Your algorithm must run in O(n) worst-case time. 

(b) Let S be a sorted array of n integers. Give an algorithm that finds the pair x, y ∈ S that maximises |x − y|. Your algorithm must run in O(1) worst-case time. 

(c) Let S be an unsorted array of n integers. Give an algorithm that finds the pair x, y ∈ S that minimises |x − y|, for x ̸= y. Your algorithm must run in O(n log n) worst-case time.
(d) Let S be a sorted array of n integers. Give an algorithm that finds the pair x, y ∈ S that minimises |x − y|, for x ̸= y. Your algorithm must run in O(n) worst-case time.

*Sol:*
a) 
> Array is unsorted, we can use two pointers to get min, max in linear time to get x, y pair. Time complexity:O(n)

b)
> Array is sorted. x should be first element of array, y should be last. In an array, both these operations take O(1) time.

c)
> Array is unsorted. We can sort it using merge sort(worst case time complexity O(nlogn)). We can traverse the sorted array linearly to find the pair. 

d)
> Same as c) after sorting as in this case we already have sorted array.

**4-3.**  Take a sequence of 2n real numbers as input. Design an O(nlogn) algorithm that partitions the numbers into n pairs, with the property that the partition minimizes the maximum sum of a pair. For example, say we are given the numbers (1,3,5,9). The possible partitions are ((1,3),(5,9)), ((1,5),(3,9)), and ((1,9),(3,5)). The pair sums for these partitions are (4,14), (6,12), and (10,8). Thus the third partition has 10 as its maximum sum, which is the minimum over the three partitions.

*Sol:* 
> 1. Sort the numbers in O(nlogn)
> 2. Assign two pointers

```
start = A[0]
end = A[2n-1]

while start < end {
	pair(start, end)
	start++
	end--
}
```
**4-4.** [3] Assume that we are given n pairs of items as input, where the first item is a number and the second item is one of three colors (red, blue, or yellow). Further assume that the items are sorted by number. Give an O(n) algorithm to sort the items by color (all reds before all blues before all yellows) such that the numbers for identical colors stay sorted.For example: (1,blue), (3,red), (4,blue), (6,yellow), (9,red) should become (3,red), (9,red), (1,blue), (4,blue), (6,yellow).

*Sol:*
The numbers are already sorted. If we can create buckets of color, and put each pair into the bucket according to their color, we will have the desired result.
> 1. Initailize three buckets(array) corresponding to red, blue yellow
> 2. Traverse the input and put the pair into buckets according to the color
> 3. Concatenate the pairs, starting from red bucket, then blue, then yellow.

**4-5.** The mode of a set of numbers is the number that occurs most frequently in the set. The set (4, 6, 2, 4, 3, 1) has a mode of 4. Give an efficient and correct algorithm to compute the mode of a set of n numbers.

*Sol:*
*Algo1*:
> 1. Create a hashmap from array with key equal to object and count(of that object in array) as value.
> 2. Traverse all keys of hashmap to get the object with most count.

> Time Complexity: O(n), Space: O(n)

*Algo2*:
> 1. Sort the array
> 2. Keep a count and object pointer. Traverse the array and keep on updating count and object pointer, if required.
> Time Complexity: O(nlogn)

**4-6.** [3] Given two sets S1 and S2 (each of size n), and a number x, describe an O(n log n) algorithm for finding whether there exists a pair of elements, one from S1 and one from S2, that add up to x. (For partial credit, give a Θ(n2) algorithm for this problem.)

*Sol:*
> 1. Sort S2.
> 2. For every element of S1, binary search the pair in S2.

```
func findPairForX:(_ x:Int, inArrays s1:[Int], s2:[Int]) -> (Int, Int)? {
	s2.sortInPlace(<)
	
	for item in s1 {
		if let pair = binarySearch(s2, x-item) {
			return (item, pair)
		}
	}	
	return nil
}

func binarySearch(s2:[Int], low:Int, high:Int, element:Int) -> Int? {
	guard low < high else {
		return nil
	}
		
	int mid = low + (high-low)/2
	if s2[mid] == x {
		return s2[mid]
	} else if(s2[mid] > x) {
		return binarySearch(s2, low, mid-1, x)
	} else {
		return binarySearch(s2, mid+1, high, x)
	} 
} 

```
> Time Complexity: Sort takes O(n logn) time. Then we have a for loop of n items and using binary search to find elements in s2. Total complexity is O(nlogn) + O(nlogn) = 2*O(nlogn) ~ O(nlogn)

**4-7.** Outline a reasonable method of solving each of the following problems. Give the order of the worst-case complexity of your methods.(a) You are given a pile of thousands of telephone bills and thousands of checks sent in to pay the bills. Find out who did not pay.(b) You are given a list containing the title, author, call number and publisher of all the books in a school library and another list of 30 publishers. Find out how many of the books in the library were published by each company.(c) You are given all the book checkout cards used in the campus library during the past year, each of which contains the name of the person who took out the book. Determine how many distinct people checked out at least one book.

*Sol:*
a) 	
> 1. Sort array of checks
> 2. Traverse array of bills and find using binary search the name of bill holder in checks(already sorted) 
> 
> Complexity: O(nlogn) + O(nlogn) ~ O(nlogn)

b)
> 1. Use author names to create buckets
> 2. Traverse list of books and put each book item in its appropriate bucket
> 3. Print the books from each bucket
> 
> Complexity: Space - O(n), Time- O(n)

c) 
This seems to be the problem of removing duplicates from a list of names. In Objective-C, we could simply have created a Set from this array. But I think Set DS is not available in every language. We can use Hashmap for removing duplicates then:
> 1. Traverse unsorted list and keep putting all names into a hashmap if name does not already exist inn hashmap.
> 
> Complexity: Space-O(n), Time-O(n)

**4-8.** [4 ] Given a set of S containing n real numbers, and a real number x. We seek an algorithm to determine whether two elements of S exist whose sum is exactly x.(a) Assume that S is unsorted. Give an O(n log n) algorithm for the problem.(b) Assume that S is sorted. Give an O(n) algorithm for the problem

*Sol:*
a) This again can be solved by a mix of binary search and sorting with complexity equal to O(nlogn)

b) We cannot employ binary search while traversing elements, because that will make time complexity O(nlogn). Good thing is that we already have a sorted array. 
*Algo:*
> 1. Initialize two pointers, left and right where left = first element and right = last element.
> 2. Check for their sum, if left+right < sum, increment left, ow increment right

```
func findPairOfElementsForGivenSum(_ x:Int, s:[Int]) -> (Int,Int)? {
	var left = 0
	var right = s.count-1
	
	while(left < right) {
		let sum = s[left]+s[right]
		
		if sum == x {
			return (s[left], s[right])
		} else if sum < x {
			left = left+1
		} else {
			right = right+1	
		}
	}
}

```
**4-9.** Give an efficient algorithm to compute the union of sets A and B, where n = max(|A|,|B|). The output should be an array of distinct elements that form the union of the sets, such that they appear more than once in the union.(a) Assume that A and B are unsorted. Give an O(nlogn) algorithm for the problem.(b) Assume that A and B are sorted. Give an O(n) algorithm for the problem.

*Sol:*

a)
> 1. Elements are unsorted. Initialize an empty set USet and fill it with all elements of Set A. Also, create a hashmap and fill with elements from A.
> 2. Traverse Set B and after checking for element in hashmap, if not found then insert in USet. This way we can achieve Union in O(n).

b)
> 1. We can use hashmap again but because elements are already sorted, we should instead use pointers to traverse the Sets A and B. 
> 2. Move Set A pointer if its elements are less than or equal to Set A, ow move Set B elements. Then add all elements which are not yet added/evaluated.

**4-10.** [5] Given a set S of n integers and an integer T , give an O(n^(k−1) log n) algorithmto test whether k of the integers in S add up to T.*Sol:*
1. If k = 1, Complexity = O(n^0 log n) ~ O(log n). For an unsorted array, it seems very difficult to find any integer in O(log n). The best we can do is linear search I guess.
2. If k = 2, Complexity = O(n^1 log n). We need two integers(k = 2) to add upto T. This problem is now exactly like 4-8. We can use the same solution.
3. If k = 3, Complexity = O(n^2 log n). We need three integers to add to T. We can use two loops. Solution for this, can be extended for all cases, k>=3.

> suppose we have an array = [2, 7, 3, 6, 1], T = 10, k = 3

> 1. sort the array, array = [1, 2, 3, 6, 7]
> 2. start outer loop, for i = 0; i< n; i++
> 3. start inner loop, for j = i+1; j< n; j++
> 4. use binary search to find element, T- array[i] - array[j]

**4-11.** Design an O(n) algorithm that, given a list of n elements, finds all the elements that appear more than n/2 times in the list. Then, design an O(n) algorithm that, given a list of n elements, finds all the elements that appear more than n/4 times.

*Sol:* 

The first part of the question can be solved by **Majority Algorithm**

A = [5, 2, 7, 2, 2, 1, 2]. N = 7, elements occurring more than N/2 ~ 3 times = 2

~~**Algo1**~~

> We can use hashmap solution, where we can create a hashmap of all elements and then traverse that hashmap to find all keys which have values N/2.  But that means space complexity is O(n)

~~**Algo2**~~

> If we know the range of the n elements, we can use part of counting sort algo, where we create an array of elements 1...k, where k is the range of elements and traverse given array to fill range array with count of each element.
0<sub>0</sub>|1<sub>1</sub>|4<sub>2</sub>| 0<sub>3</sub> | 0<sub>4</sub> | 1<sub>5</sub>|0<sub>6</sub>|1<sub>7</sub>
Ofcourse this would work only if range of elements in n are restricted in a range ow we are going to waste a lot of space. Also, space complexity here is O(k).

**Algo3 : Moore's Voting Algorithm**

Note that there can be at most one element which appears more than n/2 (i.e. at least ceil(n/2) ) times in the list.

Moore's Voting Algo is a two step process:

1. Get an element occurring most of the time in the array. This phase will make sure that if there is a majority element then it will return that only.
2. Check if the element obtained from above step is majority element.

```
func findRepeatingElementIn(_ array:[Int]) -> Int? {
    var result:Int? = nil
    
    guard array.count > 1 else {
        return nil
    }
    
    var count:Int = 1 // Initial count
    var majorityIndex:Int = 0 // Initial Majority Index
    
    for index in 1..<array.count { // Iteration
        if array[index] == array[majorityIndex] { // If item matches
            count += 1
        } else {
            count -= 1 // If item does not match
        }
        
        if count <= 0 { // If count <= 0, then reset
            majorityIndex = index
            count = 1
        }
    }

    if majorityIndex >= 0 { // After we have found majorityIndex, we need to make sure that that element is repeating more than n/2 times
        count = 0
        for item in array {
            if(item == array[majorityIndex]) {
                count += 1
            }
        }
        
        if count > array.count/2 {
            result = array[majorityIndex]
        }
    }
    
    return result
}

```

---

*Heaps*

**4-12.** Devise an algorithm for finding the k smallest elements of an unsorted set of n integers in O(n + k log n).

*Sol:* The complexity points us to use Min heap and use extractMin() method *k* times and then heapify.
> O(n) is complexity for creating min heap
> O(logn) is complexity for extractMin(because of heapify)

**4-13.** [5] You wish to store a set of n numbers in either a max-heap or a sorted array. For each application below, state which data structure is better, or if it does not matter. Explain your answers.(a) Want to find the maximum element quickly. 
(b) Want to be able to delete an element quickly.(c) Want to be able to form the structure quickly. 
(d) Want to find the minimum element quickly.
 
*Sol:*
a) same for both hence does not matter
b) heap because after deletion, restructuring takes O(logn) 
c) Heap can be formed in o(n) time
d) finding min element in max heap requires visiting each leaf node whereas in array it is O(1).
 
**4-14.**[5] Give an O(n log k)-time algorithm that merges k sorted lists with a total of n elements into one sorted list. (Hint: use a heap to speed up the elementary O(kn)- time algorithm).
 
*Sol:*
> 1. Create initial heap from first element(minimum) from every list. Because there are k list, it means heap will have k elements and will take O(k) time to build. 
> 2. Extract minimum from heap and put that in the result array. 
> 3. Add element into heap from one of the sorted list.
> 4. Repeat step 2 until heap is empty and all sorted list elements are processed


**4-15.** [5] a) Give an efficient algorithm to find the second-largest key among n keys. You can do better than 2n − 3 comparisons.b) Then, give an efficient algorithm to find the third-largest key among n keys. How many key comparisons does your algorithm do in the worst case? Must your algorithm determine which key is largest and second-largest in the process?

*Sol:*
> 1. It is an interesting problem. The 2n-3 comparisons is worst case if we take keep two pointers and keep updating them while traversing the array. So, we cant use that solution.
> 2. We can create max heap, this will take <= 2n comparisons (remember the build max heap algorithm) and then either 2nd element or third element is the 2nd largest, hence it takes 2n+1 comparisons in worst case.
> 3. It means we cannot use heap as well.  I searched on Stackoverflow, and found [this](http://stackoverflow.com/questions/3628718/find-the-2nd-largest-element-in-an-array-with-minimum-number-of-comparisons).
> 
> The solution is asking to consider it as a tournament, where we keep on comparing the winners among each other. This means we will have n-1 comparisons and every element is involved at most log n times.
> Then to get second-largest in the process, we would need to compare the log n elements which have lost to the winner(because second-largest would have lost to thew winner and no one else). This makes log n-1 comparisons.

---

*Quicksort*

**4-16.** [3] Use the partitioning idea of quicksort to give an algorithm that finds the median element of an array of n integers in expected O(n) time. (Hint: must you look at both sides of the partition?)

*Sol:*
> 1. This problem is like finding kth largest element in an unsorted array using quicksort, only in this case k is n/2. 
> 2. Split array in two using quicksort, this would make the pivot take its sorted position. If pivot element index is less than k, then perform quicksort in array right side of pivot.
> 3. If pivot element index is greater than k, then quickort array left side of pivot.

The overall complexity for this is O(n).


---

*Interview Problems*

**4-40.** [3] If you are given a million integers to sort, what algorithm would you use to sort them? How much time and memory would that consume?
*Sol*:
1. In practice, if we have million integers to sort and they are saved in an array in RAM, it is better to use an in-place sort like randomised quick sort.
2. randomised quicksort takes O(1) space and O(nlogn) time in average case.

**4-42.** [3] Implement an algorithm that takes an input array and returns only the uniqueelements in it.
*Sol:*
**Algo1**
> 1. Create a hash map for elements 
> 2. remove element from array if on iteration it is present in hashmap.

This solution will use space O(n) and time O(n). Is there a solution that does not use hashmap?

**Algo2** from [SO](http://stackoverflow.com/questions/1532819/algorithm-efficient-way-to-remove-duplicate-integers-from-an-array)

Use hash function and a large array
>1. Reorder the array by putting elements to the indices equal to their hash values 
>2. as duplicates will come, and indices of hashvalue will already be occupied, remove them.

This is like using hashmap.

As I read on SO, this question can have many solutions, like using merge sort and during the merge subroutine, remove the duplicates.

The best answer is hashmap solution but if not there are constraints in using that, than the intreviewer wants to check the thought process.

**4-43.** [5] You have a computer with only 2Mb of main memory. How do you use it to sort a large file of 500 Mb that is on disk?
*Sol:* 
**External Sorting** to the rescue. Most precisely, External Merge Sort.

In this type of sort, we can use merge sort to sort chunk of 2mb data, and save it to the disk and then keep doing it to sort other chunks.
Then use merge sort to combine subarrays, only loading necessary data into memory.(???).

This last part was not clear to me. How can we merge chunks from these files to the bigger ones if they will not fit to the memory?  

The answer was found in this [link](http://faculty.simpson.edu/lydia.sinapova/www/cmsc250/LN250_Weiss/L17-ExternalSortEX2.htm).
It uses heap to sort the already sorted subarrays. The heap is created from the first elements of heap tree and then extract-min method is used to get the minimum element. 

This solution is like of that horse race solution, where we compare the winner horses only.(There was a similar constraint where we can not have more than 5 horses in a single race).

```
Ta1: 17, 3, 29, 56, 24, 18, 4, 9, 10, 6, 45, 36, 11, 43

Assume that we have three tapes (k = 3) and the memory can hold three records.

Main memory sort
The first three records are read into memory, sorted and written on Tb1, 
the second three records are read into memory, sorted and stored on Tb2, 
finally the third three records are read into memory, sorted and stored on Tb3.
Now we have one run on each of the three tapes:

Tb1: 3, 17, 29

Tb2: 18, 24, 56

Tb3: 4, 9, 10

The next portion of three records is sorted into main memory 
and stored as the second run on Tb1:

Tb1: 3, 17, 29, 6, 36, 45

The next portion, which is also the last one, is sorted and stored onto Tb2:

Tb2: 18, 24, 56, 11, 43

Nothing is stored on Tb3.

Thus, after the main memory sort, our tapes look like this:

Tb1: 3, 17, 29, | 6, 36, 45,

Tb2: 18, 24, 56, | 11, 43

Tb3: 4, 9, 10

Merging
B.1. Merging runs of length M to obtain runs of length k*M

In our example we merge runs of length 3 
and the resulting runs would be of length 9.

We build a heap tree in main memory out of the first records in each tape. 
These records are: 3, 18, and 4.
We take the smallest of them - 3, using the deleteMin operation, 
and store it on tape Ta1.
The record '3' belonged to Tb1, so we read the next record from Tb1 - 17, 
and insert it into the heap. Now the heap contains 18, 4, and 17.

The next deleteMin operation will output 4, and it will be stored on Ta1.
The record '4' comes from Tb3, so we read the next record '9' from Tb3 
and insert it into the heap. 
Now the heap contains 18, 17 and 9.

Proceeding in this way, the first three runs will be stored in sorted order on Ta1.
Ta1: 3, 4, 9, 10, 17, 18, 24, 29, 56

Now it is time to build a heap of the second three runs. 
(In fact they are only two, and the run on Tb2 is not complete.)

The resulting sorted run on Ta2 will be:

Ta2: 6, 11, 36, 43, 45

This finishes the first pass.

B.2. Building runs of length k*k*M

We have now only two tapes: Ta1 and Ta2. 
We build a heap of the first elements of the two tapes - 3 and 6, 
and output the smallest element '3' to tape Tb1.
Then we read the next record from the tape where the record '3' belonged - Ta1, 
and insert it into the heap.
Now the heap contains 6 and 4, and using the deleteMin operation 
the smallest record - 4 is output to tape Tb1.
Proceeding in this way, the entire file will be sorted on tape Tb1.

Tb1: 3, 4, 6, 9, 10, 11, 17, 18, 24, 29, 36, 43, 45, 56

The number of passes for the multiway merging is logk(N/M). 
In the example this is [log3(14/3)] + 1 = 2.

```

**4-44.** [5] Design a stack that supports push, pop, and retrieving the minimum element in constant time. Can you do this?

*Sol:*

We can use a minumum element variable in the stack to save the current minimum element and update it in push. 

But, on pop if the current minimum element is the one getting popped, then we need to find the next minumum element. We cannot traverse, that will make pop, O(n). 

The solution should be, that popped element should tell about that element, which was the minimum element when it was inserted. We can use another Stack(or array) to save that info.

> This will use extra space, O(n)

```
struct Stack<T:Comparable> {
    var array:Array<T> = []
    
    var sortedArray:Array<T> = []
    
    mutating func push(_ element:T) {
        array.append(element)
        
        if sortedArray.count == 0 {
            sortedArray.append(element)
        } else if let lastSortedElement = sortedArray.last {
            if lastSortedElement > element {
                sortedArray.append(element)
            } else {
                sortedArray.append(lastSortedElement)
            }
        }
    }
    
    mutating func pop(_ element:T) -> T? {
        let last = array.popLast()
        sortedArray.popLast()
       return last
    }
    
    func getMin() -> T? {
        return array.last
    }
}


```
> 1. Insert 18, S: `18`					| Sorted Stack: `18`
> 2. Insert 19, S: `18|19` 				| Sorted Stack: `18|18`
> 3. Insert 29, S: `18|19|29`				| Sorted Stack: `18|18|18`
> 4. Insert 15, S: `18|29|18|15`			| Sorted Stack: `18|18|18|15`

**There is another way to design the stack, where we don't even have to use the extra space. I found [this](http://www.geeksforgeeks.org/design-a-stack-that-supports-getmin-in-o1-time-and-o1-extra-space/) solution in Geekforgeeks.**

**4-46.** You are given 12 coins. One of them is heavier or lighter than the rest. Identify this coin in just three weighings.

Note - weighings are with a balance, where you can have a greater than, equal to, or less than result. You can't do this with a digital scale.

*Sol:*>1. Divide into 4 groups of 3 each.
2. **Weigh 1**: Weigh any 2 groups.
3. If step 2 balances, the light or heavier coin is in one of the other two groups
    If it doesn't balance, the different coin is within one of these two groups
4. **Weigh 2**: Weigh any one grp of step 3 with third group of coins from step 1.
5. If step 4 balances, the group you just moved out from scale has the different coin.
    If it doesn't balance, the group you just put on the scale in step 4 has different coin.
In step 5, depending upon the scale, you would also get to know if the coin is heavier or lighter from others.
6. Now you know which 3 balls has one different coins. 	
    **Weigh 3**: Take any two coins of this group and weigh on scale.
    If it balances, the third coin is heavier or lighter.
    If it doesn't balance, you know in step 5 if it's heavier or lighter. Choose the side of scale accordingly and pick the heavy/light coin.




