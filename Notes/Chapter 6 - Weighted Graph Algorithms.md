## Weighted Graph Algorithms

There is different set of problems and algorithms for weighted graphs. In this chapter we are going to read about minimum spanning trees, shortest paths and maximum flows.

### 6.1 Minimum Spanning Trees (MST)
A spanning tree of a graph G = (V, E) is a subset of edges forming a tree connecting all vertices. The minimum spanning tree does it in minimum sum of edge weights.

==In real life, lets say we want to connect all homes with wire. Minimum Spanning tree is the answer for that.==

There can be more than one spanning tree in a graph. In an unweighted graph, all spanning trees are minimum spanning trees and can be found using DFS or BFS. 

In weighted graphs, finding minimum spanning trees are a little difficult. Following are two algos given to find minimum spanning tree in a weighted graph. Both are somewhat greedy in nature.

#### 6.1.1 Prim's Algorithm
In Prim's algo, any one vertex is selected and then selecting the nearest edge repeatedly, it includes all vertices.

Its a greedy algo, thus the decision to select the nearest edge is made by looking at the local options and it does not think about the whole structure.

This looks fishy? We have seen greedy algos don't always work.


######Implementation 1

Skiena gave an O(n^(2)) implementation. This implementation avoids the need to test all m edges on each pass. It only considers the ≤ n cheapest known edges represented in the parent array and the ≤ n edges out of new tree vertex v to update parent. The implementation can be improved by printing the edges as they are found and by totaling the weights of all selected edges.

```
prim(graph *g, int start){    int i;                 	/* counter */    edgenode *p;			/* temporary pointer */    bool intree[MAXV+1]; 	/* is the vertex in the tree yet? */    int distance[MAXV+1]; 	/* cost of adding to tree */    int v;					/* current vertex to process */    int w;					/* candidate next vertex */    int weight; 			/* edge weight */    int dist; 				/* best current distance from start */	for (i=1; i<=g->nvertices; i++) {        intree[i] = FALSE;
        distance[i] = MAXINT;		parent[i] = -1;
	}
	
	distance[start] = 0;	v = start;
	
	while (intree[v] == FALSE) {       intree[v] = TRUE;       p = g->edges[v];       while (p != NULL) {              w = p->y;              weight = p->weight;              if ((distance[w] > weight) && (intree[w] == FALSE)) {
              	distance[w] = weight;      				parent[w] = v;				}
				p = p->next;      	}	
      	v = 1;		dist = MAXINT;		for (i=1; i<=g->nvertices; i++) {
			if ((intree[i] == FALSE) && (dist > distance[i])) {        		dist = distance[i];				v = i; 
			}
		}	} 
}

```
######Implementation 2

The Prim's Algo can be further improved to O(m+nlogn) by using priority queue(heap).
In Prim's algo there is a step where we need to find the nearest vertex. This step takes O(n) if we use normal array but can be improved to O(logn) by using priority queue.

Here is a Swift implementation of the same:

```

// Initialize the values to be returned and Priority Queue data structure.
var cost: Int = 0
var tree = Graph<T>()
var visited = Set<T>()

// In addition to the (neighbour vertex, weight) pair, parent is added for the purpose of printing out the MST later.
// parent is basically current vertex. aka. the previous vertex before neigbour vertex gets visited.
var priorityQueue = PriorityQueue<(vertex: T, weight: Int, parent: T?)>(sort: { $0.weight < $1.weight }) // The sort closure enusres we always get the minimum weight edge at top of priority queue

// Start from any vertex
priorityQueue.enqueue((vertex: graph.vertices.first!, weight: 0, parent: nil))

// Take from the top of the priority queue ensures getting the least weight edge.
while let head = priorityQueue.dequeue() {
  let vertex = head.vertex
  if visited.contains(vertex) {
    continue
  }

  // If the vertex hasn't been visited before, its edge (parent-vertex) is selected for MST.
  visited.insert(vertex)
  cost += head.weight
  if let prev = head.parent { // The first vertex doesn't have a parent.
    tree.addEdge(vertex1: prev, vertex2: vertex, weight: head.weight)
  }

  // Add all unvisted neighbours into the priority queue.
  if let neighbours = graph.adjList[vertex] {
    for neighbour in neighbours {
      let nextVertex = neighbour.vertex
      if !visited.contains(nextVertex) {
        priorityQueue.enqueue((vertex: nextVertex, weight: neighbour.weight, parent: vertex))
      }
    }
  }
}

```

#### 6.1.2 Kruskal's Algorithm
Kruskal's Algo is also greedy, but unlike Prim's Algo it does not start with a particular vertex.
