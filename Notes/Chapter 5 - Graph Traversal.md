## Graph Traversal

* A graph G = (V, E) where V is set of vertices and E is set of edges(or vertex pairs).
* Designing truly new graph algos is very difficult. It is always better to model your existing problem to take advantage of existing algorithms.
* In this chapter, Skiena is talking about basic graph data structures and traversal operations.

### 5.1 Flavors of Graphs
The DS used to represent graphs and algorithms available to analyze them, are impacted by several fundamental properties of graphs. The first step in any graph problem is determining the flavors of graphs:

1. *Undirected*: A graph G = (V, E) is undirected if edge (x, y) ∈ E implies that (y, x) is also in E. e.g road network between cities.

	*Directed*: If not, we say graph is directed e.g. street networks within cities(because many time streets are one-way).

2. *Weighted*: Each edge(or vertex) in weighted graph G is assigned a numerical value, or weight. e.g. road networks edges having weighted speed limit, drive-time.

	*Unweighted*: In unweighted graph, there is no cost distinction between various edges and vertices.

3. *Nonsimple*: Certain types of edges complicate the task of working with graphs. A self-loop is an edge (x, x) involving only one vertex. An edge (x, y) is a multi-edge if it occurs more than once in the graph. Both these structures require special case in implementing graph algos, hence are called as nonsimple.

	*Simple*: Any graph that avoids such structures are simple.
	
4. *Sparse*: Graphs where number of actual edges(between vertex pairs) are quite less than the possible edges. 

	*Dense*: Graphs where a large fraction of the vertex pairs define edges are called dense.

5. *Acyclic*: Does not contain any cycles e.g. Trees. 
	
	*Cyclic*: Graphs which contain cycles.
	
6. *Embedded*: A graph is embedded if the vertices and edges are assigned geometric positions.

7. *Implicit*: Certain graphs are not explicitly constructed and then traversed, but built as we use them. A good example is in backtrack search.

8. *Labeled*: Each vertex is assigned a unique name or identifier in a labeled graph to distinguish it from all other vertices

![](images/flavorsofgraph.png)

### 5.1.1 The Friendship Graph

* The Friendship graph is a graph where people represent vertices and there is an edge between them, only if they are friends. This is also called social network.
* Many interesting aspects of people and their *behavior* can be studied using this.
* We can use flavors of graph, to understand Friendship graph.

1. *If I am your friend does that mean you are my friend as well?*

	This question really asks wether the graph is directed or undirected? In case of friendship, we can say that graph is perhaps undirected. The "heard of" graph is directed, because I have heard of many famous people, but they surely would not have heard of me.

2. *How close a friend are you?*

	In weighted graphs, we have a weight associated with edge. You can use this weight to measure the friendship.
	
3. *Am I my own friend?*

	This is asking whether graph is simple or nonsimple.

4. *Who has the most friends?*

	The degree of a vertex is the number of edges adjacent to it. 

5. *Do my friends live near me?*
	
	 A full understanding of social networks requires an embedded graph, where each vertex is associated with the point on this world where they live.
	
**Graphs can be used to model a variety of structures and relationships.**

### 5.2 Data Structures for Graphs

![](images/GraphDS.png)

Assume Graph G = (V, E) where V is no. of vertices and E is no. of edges

1. **Adjacency Matrix**: n x n matrix M, used to represent Graph, where M[i, j] = 1 if (i, j) is an edge in Graph. 

	*Advantages*: Fast answer to question like "Is (i, j) in G?" and edge insertion and deletion is fast as we just need to change the corresponding value in (i,j) in matrix.
	*Disadvantages*: Use of excessive space for G with many vertices and relatively less edges.

2. **Adjacency Lists**: We can more efficiently represent sparse graphs by using linked lists to store the neighbours adjacent to each vertex. 
Adjacency Lists make it harder to verify wether (i, j) is an edge in G, since we need to search through appropriate list to find edge. However, there are simple graph algos that avoid any need of such queries. We can sweep all edges of graph in one pass using breadth-first search or depth-first search and update all the actual edges as we visit it.

Comparison |   Winner 
-----------|----------
Faster to test if (x,y) is in graph? | Adjacency Matrices
Faster to find degree of vertex? | Adjacency Lists
Less memory on small graphs? | Adjacency Lists (m+n) vs n^2
Less memory on big graphs? | Adjacency Matrices 
Edge insertion or deletion? | Adjacency matrices O(1) vs O(d)
Faster to traverse the graph? | Adjacency lists Θ(m+n) vs n^2
Better for most problems? | Adjacency lists

> ==**Adjacency Lists are the right DS for most graph problems**==

**How can we implement Adjacency Lists in code?**

We can implement AdjacencyLists in many different ways. A few popular approaches are:

1. Array of arrays: The outer array represents vertices, providing an index. The inner array contains edges.
2. Array of LinkedLists: Each index in the array represents a vertex which saves a linkedlist. 
3. Dictionary of arrays: Each key in the dictionary is a vertex 


Below is an implementation of Adjacency List as array of LinkedLists.

```
#define MAXV 1000 // max vertices

typedef struct {
	int y;  						// adjacency info
	int weight; 					// weight of edge
	struct edgeNode* next;			// next edge in list
} edgeNode;

typedef struct {
	int nVertices;  				// no. of vertices in graph
	int nEdges;						// no.of edges
	edgeNode* edges[MaxV+1];		// array of edges
	bool isDirected;				// is graph directed	?
	int degree[MaxV+1];				// outdegree of each vertex
} graph;

```

```

initializeGraph(Graph* g, bool directed) {
	g->nVertices = 0;
	g->nEdges = 0;
	g->isDirected = directed;
	for(i = 0; i< MAXV+1; i++) {g->degree[i] = 0;}
	for(i = 0; i< MAXV+1; i++) {g->edges[i] = NULL;}
}

readGraph(Graph*g, bool directed) {

	int numVertices; // num of vertices
	int numEdges; // num of edges
	int x, y; // vertices in edge (x,y)
	
	initializeGraph(g, directed);
	
	scanf("%d %d", &numVertices, & numEdges);
	
	g->nVertices = numVertices;
	
	for(int i = 0; i < numEdges; i++) {
		scanf("%d %d",&x,&y);       insertEdge(g,x,y,directed);
	}
}

insertEdge(Graph* g, int x, int y, bool directed) {
	edgenode* p;
	p = malloc(sizeof(edgeNode));
	p->weight = NULL;
	p->y = y;
	p->next = g->edges[x];
	
	g->edges[x] = p // insert at head of list
	g->degree[x] = g->degree[x]+1
	
	if(directed == false) {
		insert_edge(g, y, x, true); // treat as directed ow it will stuck in loop
	} else {
		g->nEdges = g->nEdges +1;
	}
}

```


### 5.5. Traversing a Graph

* Most fundamental problem in a graph is to visit each edge and vertex in a graph in a systematic way.
* The key idea behind graph traversal is to mark each vertex when we visit it. 
* We can use enumerated types(enum) to mark the vertex. Each vertex will exist in one of the three states:
	1. *undiscovered*: initial state
	2. *discovered*: vertex has been discovered but we have not yet checked all its incident edges.
	3. *processed*: all incident edges are also visited.
* We must maintain a structure for all vertices that are discovered but not yet processed. Initially only the start vertex will be discovered. To completely explore a vertex, we must evaluate each edge leaving v. If edge goes to undiscovered vertex x, we mark x as discovered, we ignore if x is already processed or discovered.

### 5.6 Breadth-First Search
![](images/BFS.png)

Breadth-First search is an algorithm for traversing or searching graph or tree data structures. It starts at a source node and explores the immediate neighbours first, before moving to next level neighbours. It can be used for both directed and undirected graphs.

We can use an enum to mark the nodes as we visit them and use queue structure for all nodes that are getting discovered.

![](images/AnimatedBFS.gif)

```
Q.enqueue(A). 		// Q = [A]
Q.dequeue()   		// A, Q = []
Q.enqueue(B)
Q.enqueue(C)   		// Q = [B, C]
Q.dequeue()  		// B, Q = [C]
Q.enqueue(D)
Q.enqueue(E)		// Q = [C, D, E]
Q.dequeue() 		// C, Q = [D, E]
Q.enqueue(G, F)		// Q = [D, E, F, G]
Q.dequeue()			// D, Q = [E, F, G]
Q.dequeue()			// E, Q = [F, G]
Q.enqueue(H)		// Q = [F, G, H]
Q.dequeue()			// F, Q = [G, H]
Q.dequeue()			// G, Q = [H]
Q.dequeue()			// H, Q = []
 
```

```
enum Visited {
	case unexplored
	case discovered
	case processed
}

func breadthFirstSearch(_ graph:Graph, source:Node) -> [String] {
	var queue = Queue<Node>() // DS to save discovered nodes
	queue.enqueue(source)
	
	var nodesExplored = [source.label] // array to save all visited node labels to return as traversal output
	source.visited = discovered
	
	while let node = queue.dequeue()  {
		node.visited = processed
		for edge in node.neighbours {
			let neighbourNode = edge.neighbour 
			if neighbourNode.visited == unexplored {
				queue.enqueue(neighbourNode)
				neighbourNode.visited = discovered
				nodesExplored.append(neighbourNode.label)
			}
		}
	}
	return nodesExplored
}


```

### 5.7 Applications of BFS
* BFS runs in O(n+m) time on both directed and undirected graphs, where n is no. of vertices and m is no. of edges.
* We need to understand when traversal approach would work:
	1. *Connected Component*: Set of vertices where there is a path between every pair of vertices. A complicated problem can reduce to finding or counting connected components such as testing whether a Rubik's cube can be solved from any position.
	2. *Two-Coloring graphs*: The vertex-coloring problem seeks to assign a color to each vertex so that no same color is assigned to vertices which are connected through an edge. A graph is bipartite, if it can be colored without any conflicts, with only two colours. We can use BFS, where if we discover a new vertex, we color it the opposite of its parent. If we found that any nondiscovery edge has the same colored vertices, it means the graph can not be bipartite.

### 5.8 Depth-First Search

* For some problems, it does not matter wether we use BFS or DFS, but for some, it becomes very important.
* The difference between BFS and DFS is the order in which we explore vertices and this order depends on the container DS we have used for saving vertices which are discovered but not yet processed.
* In BFS we use Queue whereas in DFS we use stack.

DFS can be implemented using recursion and that eliminates the need to maintain Stack.

*Algo*:

```
DFS(G, u)
	state[u] = "Discovered"
	entry[u] =  time
	time = time+1
	
	for each vertex v adjacent to u do 
		if state[v] == undiscovered then 
			p[v] = u
			DFS(G, v)
	
	state[u] = "Processed"
	exit[u] = time
	time = time+1 
	 
```
This algo has a notion of traversal time for each vertex. The time intervals have useful properties:

* *Who is an ancestor?*
 If x is ancestor of y, then we must enter x before y and exit y before x hence time interval of y must be properly nested within ancestor x.
	
* *How many descendants?* 
	The difference between the exit and entry times for u tells us how many descendants u has in the DFS. The clock gets incremented on each vertex entry and exit, it means half the time difference denotes number of descendants of a vertex.

**DFS organizes vertices by entry/exit times, and edges into tree and back edges. This organization is what gives DFS its real power.**

![](images/AnimatedDFS.gif)

```
func depthFirstSearch(_ graph:Graph, source:Node) -> [String] {
	var nodesExplored = [source.label]
	
	source.visited = discovered
	
	for edge in source.neighbours {
		let node = edge.neighbourNode
		if node.visited == unexplored {
			nodesExplored += depthFirstSearch(graph, node)
		}
		source.visited = processed
	}
	return nodesExplored
}

```

==**The complexity for both BFS and DFS is O(V+E), where V is no. of vertices and E is no. of edges.**==

### 5.9 Applications of DFS

1. **Finding Cycles in an Undirected graph**:

	The graph has a cycle if in DFS, for any visited vertex v, there is an adjacent vertex u such that u is already visited and u is not parent of v. We can keep track of parent with DFS to check the condition.
	
	In an undirected graph, we can find cycle using either BFS or DFS but in a directed graph, we can find a cycle using DFS only.
	
	**Finding Cycles in a Directed graph**:

	There is a cycle if there is a back edge present in the graph. A back edge is an edge that is from a node to itself or to one of its ancestor. 
	To detect a backedge we can keep track of vertices that are currently in our recursion stack of DFS traversal. If somehow we reach the vertex, which is already visited and that is already in the recursion stack, we say that the graph is cyclic.
	
	**Why BFS is not used to find cycle in directed graph?**
	
	Because if Graph is directed, you have to not just remember if you have visited the node or not, but also how you got there. Otherwise you might think you have found a cycle but in reality all you have is two separate paths A->B but that doesn't mean there is a path B->A
	
2. **Articulation Vertices**:
	![](images/ArticulationVertex.png)
	There is a single node, whose deletion disconnects a connected component of the graph. Such node is called articulation vertex or cut node. It is important to understand the importance of articulation vertex in actual world. Lets say there is a war, and we want to severe the telephone n/w. The articulation is the weakest point in a graph, and deleting/destroying that point disconnects the graph.
	
	There are *two algos* to search for such a vertex. 
	1. In Brute force solution, we can temporarily delete each vertex and then do a BFS or DFS of the graph to see wether graph is still connected. Complexity for such an algo is O(V(V+E))
		V+E = BFS
		(V+E)*V = because we are deleting every vertex and then adding it again
		
	2. There is a clever linear time algo as well. It requires only one DFS. 
	In DFS, we follow vertices in tree form, known as DFS tree. In DFS tree, a vertex u is articulation vertex if,
	> 1. A vertex u is root of the tree and it has at least two children. Because if both those children are connected then they would have been part of the same subtree.
	> 2. A vertex u is not root of the DFS tree but it has a child v such that no vertex in subtree rooted with v has a back edge to one of the ancestors of u. 
	> 
	> 	An edge which connects a vertex to another vertex which has a lower level is called a back edge. Presence of a back edge means presence of an alternative path in case the parent of the vertex is removed. 
	> 
	>  A leaf in a DFS tree can never be an articulation vertex. Obviously, because cutting that vertex does not disconnect the graph.
	
		So ultimately it all converges down to finding a back edge for every vertex. For that apply a DFS and record the discovery time of every vertex and maintain for every vertex v the earliest discovered vertex that can be reached from any of the vertices in the subtree rooted at v. 
		If the vertex that can be reached from the subtree vertices has discovery time equal to u or greater than u, then u is the AV.
		
		![](images/AVCases.png)
		
		* *Root Cut-Nodes:* If the root of the DFS tree has two or more children, it must be an articulation vertex. 
		* *Bridge Cut-Nodes:* If earliest reachable vertex from v is v(itself), then clearly parent of v is AV. Also, if vertex v is not a leaf then it is also a AV.
		* *Parent Cut-Nodes:* If the earliest reachable vertex from v is the parent of v, then deleting the parent must sever v from the tree unless the parent is the root. 
		
		 
		**I have changed the implementation of DFS from raywenderlich  swift club algo to support finding of articulation vertex.**
		
```
func findArticulationVertices(source: Node)  {
    
    source.visited = .discovered
    time = time+1
    source.entryTime = time
    source.reachableAncestor = source
    
    for edge in source.neighbors {
        if edge.neighbor.visited == .undiscovered {
            edge.neighbor.parent = source
            edge.processEdge(source: source)
            findArticulationVertices(source: edge.neighbor)
        } else if edge.neighbor.visited == .discovered {
            edge.processEdge(source: source)
        }
    }
    
    processVertexLate(source: source)
    time = time+1
    source.exitTime = time
    source.visited = .processed
}

func processVertexLate(source:Node)  {
    if source.parent == nil, source.outDegree > 1 { // root node
        print("root cut-node:\(source.label)")
        return
    }
    
    if let reachableAncestor = source.reachableAncestor, reachableAncestor == source.parent, let _ = source.parent?.parent {
        print("parent cut-node:\(reachableAncestor.label)")
    }
    
    if let reachableAncestor = source.reachableAncestor, source == reachableAncestor {
        if let parent = source.parent {
            print("bridge cut-node:\(parent.label)")
        }
        if source.outDegree > 0 {
            print("bridge cut-node:\(source.label)")
        }
    }
    
    let time_v = source.reachableAncestor?.entryTime ?? 0
    let time_parent = source.parent?.reachableAncestor?.entryTime ?? 0
    
    if time_v < time_parent {
        source.parent?.reachableAncestor = source.reachableAncestor
    }
}


extension Edge {
    enum EdgeType {
        case tree
        case back
        case forward
        case cross
        case none
    }
    
    func edgeType(source:Node) -> EdgeType {
        if let parent = neighbor.parent, parent == source {
            return .tree
        }
        
        if neighbor.visited == .discovered {
            return .back
        }
        
        if neighbor.visited == .processed, neighbor.entryTime > source.entryTime {
            return .forward
        }
        
        if neighbor.visited == .processed, neighbor.entryTime < source.entryTime {
            return .cross
        }
        
        return .none
    }
    
    func processEdge(source:Node) {
        switch(edgeType(source: source)) {
        case .tree:
            source.outDegree += 1
        case .back where source.parent != neighbor:
            if let reachableAncestorSource = source.reachableAncestor, neighbor.entryTime < reachableAncestorSource.entryTime {
                source.reachableAncestor = neighbor
            }
        default:
            break
        }
    }
}

		
```

### 5.10 DFS on Directed Graphs

![](images/EdgeTypes.png)

* While traversing undirected graphs, an edge can either be a tree edge or a back edge. It can not be forward edge or cross edge(Why?). Because, in undirected graph, a forward edge would be declared back edge first and a cross edge would be declared a tree edge.
* In directed graph DFS, there is possibility of all four types of edges.
* The method to find an edge type is 

```
func edgeType(source:Node) -> EdgeType {
        if let parent = neighbor.parent, parent == source {
            return .tree
        }
        
        if neighbor.visited == .discovered {
            return .back
        }
        
        if neighbor.visited == .processed, neighbor.entryTime > source.entryTime {
            return .forward
        }
        
        if neighbor.visited == .processed, neighbor.entryTime < source.entryTime {
            return .cross
        }
        
        return .none
    }

``` 

### 5.10.1 Topological Sorting

![](images/TopologicalSorting.png)

* Topological sorting is the most important operation on directed acyclic graphs (DAGs). 
* It orders the vertices on the line such that all directed edges go from left to right. 
* Each DAG has atleast one topological sort. 
* The importance of Topological sort is that it gives us an ordering to process each vertex before any of its successors. 
* Applications of Topological sorting is many. Suppose we seek the shortest(or longest) path from x to y in a DAG. No vertex appearing after y in topological sort can contribute in the path because there is no way to go back to y.
* A directed graph is a DAG, if and only is no back edge is encountered. 
* Topological sorting can be performed using DFS. 

**Q: How DFS is used for Topological Sorting?**
Ans: In DFS, we start from a vertex, we first print it and then recursively call DFS for all its adjacent vertices. 
In Topological sorting, we dont print the vertex immediately. We first recursively call topological sorting for all its adjacent vertices, then push it in a stack and finally print contents of stack.	

```

process_vertex_late(int v){        push(&sorted,v);}
process_edge(int x, int y){	int class;	/* edge class */	class = edge_classification(x,y);	if (class == BACK)       printf("Warning: directed cycle found, not a DAG\n");
}

topsort(graph *g){	int i;	init_stack(&sorted);	for (i=1; i<=g->nvertices; i++)        if (discovered[i] == FALSE)                dfs(g,i);	print_stack(&sorted);       /* report topological order */
}

```

### 5.11 Exercises

*Simulating Graph Algorithms*

**5-1.** [3] For the following graphs G1 (left) and G2 (right):

![](images/graph_5_1.png)

(a) Report the order of the vertices encountered on a breadth-first search starting from vertex A. Break all ties by picking the vertices in alphabetical order (i.e., A before Z).(b) Report the order of the vertices encountered on a depth-first search starting from vertex A. Break all ties by picking the vertices in alphabetical order (i.e., A before Z).

*Sol:*
a) BFS(G1):

```
Q = [A]

1. dequeue(A) -> Q = [A], R = A 
2. enqueue -> Q = [B, D, I]
3. dequeue(B) -> Q = [D, I], R = A->B
4. enqueue -> Q= [D, I, C, E]
5. dequeue(D)-> Q= [I, C, E], R = A->B->D
6. enqueue -> Q= [I, C, E, G]
7. ...
8. ...

result = A->B->D->I->C->E->G->J->F->H

```

BFS(G2):

```
R = A

1. A, [B, E]
2. A->B, [E, C, F]
3. A->B->E, [C, F, I]
4. A->B->E->C, [F, I, D, G]
5. A->B->E->C->F, [I, D, G, J]
6. A->B->E->C->F->I, [D, G, J, M]
7. A->B->E->C->F->I->D,  [G, J, M, H]
8. A->B->E->C->F->I->D->G, [J, M, H, K]
9. A->B->E->C->F->I->D->G->J, [M, H, K, N]
10.A->B->E->C->F->I->D->G->J->M, [H, K, N]
11.A->B->E->C->F->I->D->G->J->M->H, [K, N, L]
12.A->B->E->C->F->I->D->G->J->M->H->K, [N, L, O]
13.A->B->E->C->F->I->D->G->J->M->H->K->N, [L, O, P]
14.A->B->E->C->F->I->D->G->J->M->H->K->N->L, [O, P]
15.A->B->E->C->F->I->D->G->J->M->H->K->N->L->O, [P]
16.A->B->E->C->F->I->D->G->J->M->H->K->N->L->O->P


```
b) DFS(G1)

```
A->B->C->E->D->G->H->F->J->I

```

DFS(G2)

```
A->B->C->D->H->G->F->E->I->J->K->L->M->N->O->P

```

**5-2** [3] Do a topological sort of the following graph G:
![](images/graph_5_2.png)
*Sol:*
A DFS based algo would give us this topological sort:
A->B->D->E->C->F->H->G->I->J
