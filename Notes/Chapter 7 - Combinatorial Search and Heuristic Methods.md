## Combinatorial Search and Heuristic Methods

Solving larger problems requires carefully pruning the search space to ensure we only look at the elements that really matter.

In this section, backtracking is introduced for listing all possible solutions. 

### 7.1 Backtracking

* Backtracking is a systematic way to iterate all possible configurations, ensuring that every configuration/option be generated only once.
* Backtracking ensures correctness by enumerating all possibilities and ensures efficiency by never visiting a state more than once.
* 