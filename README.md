# README #


### What is this repository for? ###

* To learn Algorithms by going through books - [Algorithm Design Manual](http://www.goodreads.com/book/show/425208.The_Algorithm_Design_Manual) and [Programming Challenges](https://www.goodreads.com/book/show/1041695.Programming_Challenges?ac=1&from_search=true) by Steven S. Skiena

* To maintain repository of problems solved(from Programming Challenges) in C++

* To maintain personal notes for course material of Skiena's 2012 [course](http://www3.cs.stonybrook.edu/~algorith/video-lectures/) and book

* To maintain solutions for Exercises given at end of every chapter

### How is repository arranged? ###

Repository contains following folders:

* **CourseMaterial**: 

    1.  Algorithm Design Manual by Skiena

    2.  Programming Challenges by Miguel and Skiena 

    3.  Skiena's StonyBrook lectures - CSE373 - Analysis of Algorithms

* **Programming Challenges**: 

    * Xcode C++ project.

    * Solutions for problems in Programming Challenges book.


* **Notes**: 

    1. Quick revision personal notes.
    2. Coding sample inside notes are written in a mix of Swift and C++. 
    3. Some of the coding samples and references are taken from Raywenderlich gitgub repo: https://github.com/raywenderlich/swift-algorithm-club/ 



### Who do I talk to? ###

* Puneet Sharma - puneet.sh2525@gmail.com