//
//  The 3n + 1 Problem.cpp
//  ProgrammingChallenges_Skiena
//
//  Created by Puneet Sharma2 on 04/03/17.
//  Copyright © 2017 Personal. All rights reserved.
//

#include <stdio.h>
#include <iostream>

// Algorithm
// 1. Use a loop to iterate the numbers between i and j including them. Start from smaller number and move to larger number
// 2. Use another loop to determine max cycle length.
// 3. Replace maxCycleLength if found greater than previous value
// 4. print i, j and max cycle length, separated by space
void printMaximumCycleLength(int i, int j) {
    int maxCycleLength = 0; // variable for calculating max cycle Length
    int n = i >= j ? i : j;
    int smallernumber = i < j ? i : j;
    while (n >= smallernumber) {
        int tempNum = n;
        int tempCycleLength = 1;
        while (tempNum != 1) {
            if(tempNum%2 == 1) { // odd number
                tempNum = tempNum*3+1;
            } else {
                tempNum = tempNum/2;
            }
            tempCycleLength++;
        }
        if (maxCycleLength<tempCycleLength) {
            maxCycleLength = tempCycleLength;
        }
        n--;
    }
    fprintf(stdout, "%d %d %d\n", i, j, maxCycleLength);
}

