//
//  JollyJumpers.cpp
//  ProgrammingChallenges_Skiena
//
//  Created by Puneet Sharma2 on 08/04/17.
//  Copyright © 2017 Personal. All rights reserved.
//

#include "JollyJumpers.hpp"
#include <iostream>

int array[3000];
int diff[3000];

void readJollyJumpersInput() {
    int n;
    
    while(scanf("%d", &n) != EOF){
        int i;
        for(i = 0; i < n; i++) {
            std::cin >> array[i];
        }
        
        for (i = 1; i < n; i++) {
            diff[i] = 0;
        }
        
        for (i = 0; i < n-1; i++) {
            int d = abs(array[i]-array[i+1] );
            if( d < 1 || d > n-1 || diff[d] == 1 )
            {
                std::cout << "Not jolly" << std::endl;
                break;
            }
            diff[d] = 1;
        }
        
        if(i == n-1 )
        {
            std::cout << "Jolly" <<std::endl;
        }
    }
    
}
