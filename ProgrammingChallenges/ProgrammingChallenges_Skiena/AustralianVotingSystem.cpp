
//  AustralianVotingSystem.cpp
//  ProgrammingChallenges_Skiena
//
//  Created by Puneet Sharma2 on 15/03/17.
//  Copyright © 2017 Personal. All rights reserved.
//

#include "AustralianVotingSystem.hpp"

struct Candidate {
    string name;
    bool isEliminated;
    int votes;
};


void countVotesInitially(int nCandidates, vector<Candidate>& candidates, int nVoters, vector<vector<int>>& ballot) {
    for(int i = 0; i < nVoters; i++) {
        int index = ballot[i][0]-1;
        candidates[index].votes++;
    }
}

bool eliminate(int nCandidates, vector<Candidate>& candidates, int nVoters, vector<vector<int>>& ballot) {
    int highest = -1; // initialize highest vote tolowest possible
    int lowest = 1001; //initailize lowestvotes to max possible
    for (int i = 0; i < nCandidates; i++) {
        Candidate c = candidates[i];
        if(c.isEliminated) {
            continue;
        }
        if(c.votes > highest) {
            highest = c.votes;
        }
        if(c.votes < lowest) {
            lowest = c.votes;
        }
        
    }
    if(highest == lowest) {
        return false;
    }
    
    for (int i = 0; i < nCandidates; i++) {
        Candidate c = candidates[i];
        if(c.votes == lowest) {
            c.isEliminated = true;
        }
        for(int i = 0; i < nVoters; i++) {
            for (int j = 1; j < nCandidates; j++) {
                int index = ballot[i][j]-1;
                if(!candidates[index].isEliminated) {
                    candidates[index].votes++;
                    break;
                }
            }
        }
    }
    return true;
}


// Checks if number of votes is greater than half.If yes, returns index of candidate else returns -1
int checkForAboveFifty(vector<Candidate> candidates, int nVoters) {
    int halfNumberOFVotes = nVoters/2;
    for (int i = 0; i < candidates.size(); i++) {
        Candidate c = candidates[i];
        if(c.votes > halfNumberOFVotes) {
            return i;
        }
    }
    return -1;
}

void printOutputOfAustralianVotingSystem(int nCandidates, vector<Candidate> candidates, int nVoters, vector<vector<int>> ballot) {
    countVotesInitially(nCandidates, candidates, nVoters, ballot);
    int winner = -1;
    do {
        winner = checkForAboveFifty(candidates, nVoters);
        if(winner != -1) {
            break;
        }
    } while(eliminate(nCandidates, candidates, nVoters, ballot));
    if (winner != -1) {
        cout << candidates[winner].name << endl;
    } else {
        for (int i = 0; i < nCandidates; i++) {
            Candidate c = candidates[i];
            if(!c.isEliminated){
                cout << c.name << endl;
            }
        }
    }
}

void readAustralianVotingInput() {
    int sampleSize; // sample size
    cin >> sampleSize;
    cin.get();
    
    while (sampleSize > 0) {
        int nCandidates; // no.candidates, nCandidates <= 20
        cin >> nCandidates;
        
        vector<Candidate> candidates;
        vector<vector<int>> ballot(1001, vector<int>(nCandidates));
        cin.get();
        for (int i = 0; i<nCandidates; i++) {
            string name;
            std::getline(std::cin, name);
            Candidate c = {name, false, 0};
            candidates.push_back(c);
        }
        cin.get();
        string voteString;
        int nVoters = 0;
        for (nVoters=0; nVoters <1000; nVoters++) {
            char input;
            cin >> input;
            if(!cin.eof() || input != '\n') { // HINT: Press ctrl+d for eof ine Xcode Console
                cin.putback(input);
                for (int i=0; i<nCandidates; i++) {
                    cin >> ballot[nVoters][i];
                }
            } else {
                break;
            }
        }
        cin.get();
        printOutputOfAustralianVotingSystem(nCandidates, candidates, nVoters, ballot);
        sampleSize--;
    }
}
