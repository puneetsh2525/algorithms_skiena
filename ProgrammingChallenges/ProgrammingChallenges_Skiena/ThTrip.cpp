//
//  ThTrip.cpp
//  ProgrammingChallenges_Skiena
//
//  Created by Puneet Sharma2 on 08/03/17.
//  Copyright © 2017 Personal. All rights reserved.
//

#include "ThTrip.hpp"


// Catch:
// To solve the problem, if we use float/double to save money the output would be wrong because of rounding errors.
// Read: http://stackoverflow.com/questions/3730019/why-not-use-double-or-float-to-represent-currency
// The challenge is when we have tricky cent values which give us rounding issues.
//


// Algorithm
// 1. Find average of all double elements in array.
// 2. Start a loop iterating all elements of array
// 3. For every student, find the difference and cut it off at the cent by multiplying by 100, converting to long and then dividing by 100.
// 4. Maintain positive and negative sum because of above calcultaion.
// 5. Print the greater number.

void printTotalAmountOfMoneyExchanged(int count, double array[count]) {
    // Find average
    double totalAmount = 0;
    int i;
    for(i = 0; i < count; i++) {
        double money = array[i];
        totalAmount += money;
    }
    
    double average = totalAmount/count;
    
    double negativeSum = 0;
    double positiveSum = 0;
    
    // Find sum
    for(i = 0; i < count; i++) {
        double money = array[i];
        double diff = (double)(long)((money-average)*100)/100; // for rounding errors, and to ensure 0.01 precision
        if(diff < 0) {
            negativeSum += diff;
        } else {
            positiveSum += diff;
        }
    }
    double totalMoneyExchanged = -(negativeSum) > positiveSum ? -(negativeSum) : positiveSum;
    printf("$%.02f\n",totalMoneyExchanged);
}

void readTripInput() {
    int count;
    while(scanf("%d", &count) != EOF){
        if(count == 0) {
            return;
        }
        double array[count];
        int i = 0;
        while (i < count) {
            double money;
            scanf("%lf", &money);
            array[i] = money;
            i++;
        }
        printTotalAmountOfMoneyExchanged(count, array);
    }
}
