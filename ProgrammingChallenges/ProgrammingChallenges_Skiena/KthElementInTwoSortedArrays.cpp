//
//  KthElementInTwoSortedArrays.cpp
//  ProgrammingChallenges_Skiena
//
//  Created by Puneet Sharma2 on 11/05/17.
//  Copyright © 2017 Personal. All rights reserved.
//

#include "KthElementInTwoSortedArrays.hpp"
#include <algorithm>

int findKthSmallest(int a[], int b[], int sizeA, int sizeB, int k) {
    
    if(sizeA==0 && sizeB>0)
        return b[k-1];
    else if(sizeB==0 && sizeA>0)
        return a[k-1];
    else if(sizeA+sizeB < k)
        return -1;
    
    if(k == 1)
        return std::min(a[0], b[0]);
    
    
    int i = std::min(sizeA, k/2);
    int j = std::min(sizeB, k/2);
    
    if(a[i-1]<b[j-1])
        return findKthSmallest(a+i, b, sizeA-i, j, k-i);
    else if (a[i-1]>b[j-1])
        return findKthSmallest(a, b+j, i, sizeB-j, k-j);
    
    return -1;
}

void problemStatement() {
    int a[] = {3, 4, 5, 6};
    int b[] = {1, 8, 29};
    int k = 2;
    int answer = findKthSmallest(a, b, 4, 3, k);
    printf("%d\n",answer);
}
